'''
Created on Apr 29, 2014

@author: abhinav
'''

import datetime
import time

INT_MAX= 1000000000
SELECT_ONE  = 0
SELECT_RANGE = 1

SECONDS_IN_DAY = 86400


MONDAY= 0
TUESDAY =1
WEDNESDAY = 2
THURSDAY = 3
FRIDAY = 4
SATURDAY =5
SUNDAY =6
time.localtime()
USER_ITEM = 0
APPROVAL_NEEDED_BO = 2
APPROVAL_NEEDED_USER = 3

ITEM_DELIVERED = 4
ITEM_INTRANSIT = 2
ITEM_PROCESSING = 1
ITEM_NOT_DELIVERED = 3

ONE_DAY= datetime.timedelta(days = 1)
  

N_PROCESSES= 1
PROCESSES_ID = 0#0-5
#__count = 0

CHOICES_CATEGORIES= [
                     "WOMENS_SHOES" , "WOMENS_APPARELS", "EYE_GLASSES","SPECTACLES"                    
                     ]


def toUtcTimestamp(dt, epoch=datetime.datetime(1970,1,1)):
    td = dt - epoch
    # return td.total_seconds()
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 1e6 
