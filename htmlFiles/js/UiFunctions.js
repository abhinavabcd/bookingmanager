function addDateRangeSelector(divId){
	var $picker = $( "<div id='datepicker-calendar' style='display:block;'></div>" );
	var $inp = $("<input class='dateinput' onclick='showDatePicker();' style='width:200px;' id='dateinput' value='' />");
	$("#"+divId).append($inp,$picker);
	var to = new Date();
	var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 14);

	$('#datepicker-calendar').DatePicker({
	  inline: true,
	  date: [from, to],
	  calendars: 3,
	  mode: 'range',
	  current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
	  onChange: function(dates,el) {
	    // update the range display
		  $("#dateinput").val(
	      dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+
	      dates[0].getFullYear()+' - '+
	      dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+
	      dates[1].getFullYear());
	  }
	});
	$(".datepickerContainer").attr('tabindex','10');
	$(".datepickerContainer").attr("onblur",'hideDatePicker();');
}

function showDatePicker(){
	$('#datepicker-calendar').toggle();
}

function hideDatePicker(){
	$('#datepicker-calendar').hide();
}

function addDateSelector(divId) {
	var $picker = $( "<div id='datepicker-calendar' style='display:block;'></div>" );
	var $inp = $("<input class='dateinput' onclick='showDatePicker();' style='width:100px;' id='dateinput' value='' />");
	$("#"+divId).append($inp,$picker);
	
	$('#datepicker-calendar').DatePicker({
		  mode: 'single',
		  inline: true,
		  onChange: function(dates,el) {
		    // update the range display
			$("#dateinput").val(
		      dates.getDate()+' '+dates.getMonthName(true)+', '+
		      dates.getFullYear());
		  	}
	});
	$(".datepickerContainer").attr('tabindex','10');
	$(".datepickerContainer").attr("onblur",'hideDatePicker();');
}

function addMultipleDateSelector(divId) {
	var $picker = $( "<div id='datepicker-calendar' tabindex='10' onblur='hideDatePicker();' style='display:block;'></div>" );
	var $inp = $("<input class='dateinput' onclick='showDatePicker();' style='width:200px;' id='dateinput' value='' />");
	$("#"+divId).append($inp,$picker);//($rangeField,$picker);

	$('#datepicker-calendar').DatePicker({
		  mode: 'multiple',
		  inline: true,
		  onChange: function(dates,el) {
		    // update the range display
			  var tmp="";
			  for(var i=0;i<dates.length;i++){
				  if(tmp!=""){
						  tmp = tmp + " ; ";
				  }
				  tmp = tmp+dates[i].getDate()+' '+dates[i].getMonthName(true)+', '+dates[i].getFullYear();
			  }
			  $("#dateinput").val(tmp);
		  	}
	});
	$(".datepickerContainer").attr('tabindex','10');
	$(".datepickerContainer").attr("onblur",'hideDatePicker();');
}

function addRangeSelector(divId,list){
	var tmp = "<select>";
	for(var i=0;i<list.length;i++){
		tmp=tmp+"<option value="+list[i]+">"+list[i]+"</option>";
	}
	tmp = tmp+"</select>";
	$("#"+divId).append($(tmp));
} 

function addListItemsList(divId,llist){
	var tmp = "";
	for(var i=0;i<llist.length;i++){
		tmp = tmp + "<button id='"+llist[i].ref+"' class='bbutton' type='button' value='"+llist[i].ref+"'>"+llist[i].name+"</button>";
	}
	$("#"+divId).append($(tmp));
}