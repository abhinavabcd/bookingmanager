/** @jsx React.DOM */

var testLink = React.createClass({
	render: function(){
		return <a>{this.props.cont}</a>;
	}
});

var CheckLink = React.createClass({
  render: function() {
    // transferPropsTo() will take any props passed to CheckLink
    // and copy them to <a>
    return <testLink cont={this.props.children}></testLink>;
  }
});

React.renderComponent(
  <CheckLink href="javascript:alert('Hello, world!');">
    Click here!
  </CheckLink>,
  document.getElementById('example')
);