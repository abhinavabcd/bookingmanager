var data = [];
var settings = [];
 
$(window).load(function() {
$('.taggd').each(function(i, e) {
var $e = $(e);
 
$e.taggd(settings[i]);
$e.taggd('items', data[i])
});
});

data.push([
{ x: 0.512, y: 0.33, text: 'Huey' },
{ x: 0.548, y: 0.33, text: 'Dewey' },
{ x: 0.567, y: 0.36, text: 'Louie' }
]);
 
settings.push({
align: { 'y': 'bottom' },
offset: { 'top': -15 },
 
'handlers': {
'mouseenter': 'show',
'mouseleave': 'hide'
}
});