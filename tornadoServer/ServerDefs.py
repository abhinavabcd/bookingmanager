'''
Created on May 14, 2014

@author: abhinav2
'''
from  BaseApplication import *
from Utils import *
import json
import datetime

class CaptureReponse():
    data =None
    response =None
    def __init__(self,responseObj):
        self.data = None
        self.response = responseObj
    def get_arguments(self ,*args, **kwargs):
        return self.response.get_arguments(*args,**kwargs)
        
    def write(self,data):
        if(not self.data):
            self.data = data
        else:
            self.data.update(data)
    def getData(self):
        return self.data
    def send(self):
        self.response.write(self.data)

def getListItemJson(response):
    ref = response.get_argument("listItemRef")
    response.write(ListItem.objects.get(ref=ref).to_json())


def getNextUserSelectionsJson(response,paths=None , selectedRanges=None):
    paths = paths if paths else json.loads(response.get_argument("userSelectionPath")) #{userSelectionRef,listItemRef,selectedRange}
    selectedranges = selectedRanges if selectedRanges else json.loads(response.get_argument("selectedRanges"))
    if(not paths):
        return
    userSelectionRef = paths[-1]["userSelectionRef"]
    childSelections = baseApplication.getUserSelectionTempStore().findUserSelectionByRef(userSelectionRef).onSelection(selectedranges)
    userSelectionNodesList = []
    for path in paths:
        userSelectionNodesList.append(UserSelectionNodes(path["selectedRange"],[],path["listItemRef"]))
    availables = []
    jsonData = []
    for i in childSelections:
        userSelectionNodesList[-1]._range =  i.parentRange
        unAvailable = baseApplication.getNodeDataStore().findAvailables(None, userSelectionNodesList , 0,  [])
        jsonData.append([i.to_json(),unAvailable])
    response.write({"childSelections":jsonData})

def loadUserSelection(response):#argument listItemRef , will load a userSelection
    listItemRef = json.loads(response.get_argument("listItemRef"))
    response.write(baseApplication.getUserSelectionTempStore().CreateNewUserSelection(listItemRef, None, None, None, None).to_json())

def loadUserSelection2(response):# the listItemRef and selectedRanges , will load the next selections , only for initial listItem Loading
    listItemRef = json.loads(response.get_argument("listItemRef"))
    selectedRanges = json.loads(response.get_argument("selectedRanges"))
    userSelection = baseApplication.getUserSelectionTempStore().CreateNewUserSelection(listItemRef, None, None, None, None)
    response2 = CaptureReponse(response)
    getNextUserSelectionsJson(response2,[[userSelection.ref, userSelection.listItemRef,[]]],selectedRanges)
    response2.send()





'''will read the json of userSelection specifically map , inserts in db , evaluates the price and saves into cart or buys it of directly if 0 price, then respond that this has been added to cart'''
@isLoggedIn
def appendToCartJson(response,user=None):
    responseJson = {}
    response.write(addToCart(json.loads(response.get_argument("userSelection")),user,responseJson))
    

'''utility function ,  recursive, because of linked selections'''
def addToCart(u1,user,responseJson):#create user selections on db
    map={}
    def parseUserSelections(userSelection):
        map[(userSelection['listItemRef'],userSelection['parentRange'])] = userSelection
        for child in userSelection['childSelections']:
            parseUserSelections(child)
    parseUserSelections(u1)
    
    q = deque()#all userselection to be saved
    def insertUserSelections(u1,userSelection):
        if(not u1):
            return False            
        refs , childSelections = userSelection.onSelection(save=False)
        q+=childSelections
        for child in childSelections:
            if(insertUserSelections(map[(child.listItemRef,child.parentRange)] , child)==False):
                return False
        for i in u1['linkedSelections']:#recursie adding linked selections to cart
            if(addToCart(i,user,responseJson)):
                return False # some error has occured
            
        return True
    
    u2 = UserSelection.CreateNew(u1['listItemRef'] , None,None,None,u1['selecetedRanges'])
    q.append(u2)
    if(insertUserSelections(u1, u2)):
        while(q):
            q.pop().save() #commit all to db
    else:
        return False # some error has occured
    
    price , approval = baseApplication.getUserSelctionDataStore().beforePayment(u2)
   
    if(price):
        Cart(userRef = user.ref , 
             userSelection = u2 ,
             summary="<<SUMMARY TODO HERE>>" , 
             expiryTime=datetime.datetime()+datetime.timedelta(minutes =7), 
             price=price,
             approvalNeeded = approval,
             transactionPending = True
        ).save()
        print "saved to cart , now pay and get this removed from the cart and win the item may be"
    else:
        baseApplication.getUserSelctionDataStore().onPayment(user.ref, u2)
        #TODO  add to mapdataStore that this item is bought / pending approval
        print "no price associated with the selection , directly move to you collected Items"
    return True
    
        
def viewFeaturedPage(response):
    
    pass

def viewListItem(response):
    pass

def viewCart(rsponse):
    pass
    