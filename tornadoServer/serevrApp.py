'''
Created on May 4, 2014

@author: abhinav
'''

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import os
from tornado.options import define, options, parse_command_line
import random
import string
from db import  *
import json
from TestCases import *
# 
#  Author : Abhinav
# 

IS_ALLOWED=0

####################config variables 
HTTP_PORT=8085
# status codes
NOT_ACTIVATED = 1
ACTIVATED = 2
NOT_AUTHORIZED = 3
OK =200
OK_AUTH=202
USER_EXISTS=203
USER_NOT_EXISTS = 204
SAVED_USER = 205
OK_IMMUTABLE = 206
FAILED=300
DUPLICATE_USER = 301

secret_auth="asdsadkjhsakjdhjksad"


##############333 config variables end





def generateKey(N=8):
    ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(N))


class bizyKart(tornado.web.Application):
    def __init__(self):
        settings = dict(
            static_path=os.path.join(os.path.dirname(__file__), "html/"),
            cookie_secret="11oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
            login_url="/auth/login",
            autoescape=None,
        )
        static_path = dict(path=settings['static_path'], default_filename='index.html')
           
        handlers = [
            (r"/func/(.*)",func),
            (r"/page/(.*)",func),
            (r"/(.*)", tornado.web.StaticFileHandler,static_path),
        ]
        tornado.web.Application.__init__(self, handlers, **settings)


from ServerDefs import *
### /func/name will call name() function  with response handler functions
funcMap ={"getListItemJson": getListItemJson}
class func(tornado.web.RequestHandler):
    def prepare(self):
        pass
    
    def get(self,name,*args,**kwargs):
        if(funcMap.get(name,None)):
            funcMap[name](self)
    
    def post(self,*args,**kwargs):
        self.get(*args,**kwargs)
        
pageMap ={"featured": viewFeaturedPage , "item": viewListItem , "cart": viewCart }
class page(tornado.web.RequestHandler):
    def prepare(self):
        pass
    
    def get(self,name,*args,**kwargs):
        if(pageMap.get(name,None)):
            pageMap[name](self)
    
    def post(self,*args,**kwargs):
        self.get(*args,**kwargs)
#class setListItem():
    
def renderListItem(userSelectionNodes): # this is to render userSelectionNodes[-1].listItemRef , possibly with/without windowing
    pass

def renderPreSelection(userSelectionNodes):
    pass

def createNewBusiness(jsonData , businessOperatorRef):
    '''input json, 
     1) listItems with its structure and rangeLists, discretizationfactor , userInputs, operatorInputs ,etc etc 
     2) path functions.
     3) afterSelection snippets ,(python codes with usage from api) , discount functions etc etc
     4) 
     Note: rangeLists can be given directly (A-B) , C , D etc. , will parse the text and convert to rangeList
     
     return ListItemRef
    '''

    
def submitUserSelections(userSelectionTreeJson):
    '''
    Save the userSelectionTree in the tempUserSelectionStore and show him payment page+approval needed/not needed
    '''
    