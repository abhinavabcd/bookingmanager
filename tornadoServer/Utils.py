'''
Created on Jun 2, 2014

@author: abhinav2
'''

from BaseApplication import *

def encodeStatusData( data , statusCode=None, errorCode=None):
    if(errorCode):
        return {"status":errorCode}
    return {"status":statusCode if statusCode else 'OK', "data":data}

def isLoggedIn(func):
    def wrapped(response,*args):
        userRef = response.get_secure_cookie("userRef")
        try:
            user = User.objects(ref=userRef).get(0)#optimize here
        except:
            response.write(encodeStatusData(errorCode='INVALID_USER'))
            return
        func(response,*args,user=user)
    return wrapped