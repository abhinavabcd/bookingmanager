'''
Created on May 14, 2014

@author: abhinav2
'''
from  BaseApplication import *#, getListItemDataStore
import cProfile
import re

def test1():
    a ="assaas"
    b= [[0,100]]
    a = ListItem()
    a.fragmentList(b, 5, 10)
    print b 
    a.fragmentList(b,10,30)
    print b
    print a.fragmentList(b, 3, 60)
    print b    

def test4():
    print "\n\nBaseApplication.binary_search_range Extensive for [[4,10],'A2',400],[[15,30],'A2',400]\n"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 1,3,key=lambda x: x[0])," [1,3]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 1,4,key=lambda x: x[0])," [1,4]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 1,5,key=lambda x: x[0])," [1,5]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 1,7,key=lambda x: x[0])," [1,7]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 1,10,key=lambda x: x[0])," [1,10]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 1,12,key=lambda x: x[0])," [1,12]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 1,15,key=lambda x: x[0])," [1,15]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 1,18,key=lambda x: x[0])," [1,18]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 1,30,key=lambda x: x[0])," [1,30]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 1,35,key=lambda x: x[0])," [1,35]"
    
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 4,7,key=lambda x: x[0])," [4,7]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 4,10,key=lambda x: x[0])," [4,10]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 4,12,key=lambda x: x[0])," [4,12]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 4,15,key=lambda x: x[0])," [4,15]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 4,18,key=lambda x: x[0])," [4,18]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 4,30,key=lambda x: x[0])," [4,30]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 4,35,key=lambda x: x[0])," [4,35]"

    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 5,7,key=lambda x: x[0])," [5,7]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 5,10,key=lambda x: x[0])," [5,10]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 5,12,key=lambda x: x[0])," [5,12]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 5,15,key=lambda x: x[0])," [5,15]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 5,18,key=lambda x: x[0])," [5,18]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 5,30,key=lambda x: x[0])," [5,30]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 5,35,key=lambda x: x[0])," [5,35]"
    
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 10,12,key=lambda x: x[0])," [10,12]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 10,15,key=lambda x: x[0])," [10,15]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 10,18,key=lambda x: x[0])," [10,18]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 10,30,key=lambda x: x[0])," [10,30]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 10,35,key=lambda x: x[0])," [10,35]"
    
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 12,15,key=lambda x: x[0])," [12,15]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 12,18,key=lambda x: x[0])," [12,18]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 12,30,key=lambda x: x[0])," [12,30]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 12,35,key=lambda x: x[0])," [12,35]"
    
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 15,18,key=lambda x: x[0])," [15,18]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 15,30,key=lambda x: x[0])," [15,30]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 15,35,key=lambda x: x[0])," [15,35]"

    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 18,30,key=lambda x: x[0])," [18,30]"
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 18,35,key=lambda x: x[0])," [18,35]"
    
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 30,35,key=lambda x: x[0])," [30,35]"
    
    print BaseApplication.binary_search_range([ [[4,10],'A2',400],[[15,30],'A2',400] ], 33,35,key=lambda x: x[0])," [33,35]"
    
    print "\n\nBaseApplication.isSubsetRange([6,7], [5,10])\n"
    print baseApplication.isSubsetRange([6,7], [5,10])
    
    print "\n\nBaseApplication.getIfRangeJoinable([6,10],[1,15])\n"
#    print BaseApplication.joinRangeLists([[1,3],[6,7]],[[4,5]])
    print BaseApplication.getIfRangeJoinable([6,10],[1,15])
    
    print "\n\nequivalentRangeLists([1,5],[1,5])\n" # possible error : if ranges in rangelists can be minified
    print BaseApplication.equivalentRangeLists([[1,5],[10,15]],[[1,3],[3,5],[10,15]])
    print BaseApplication.equivalentRangeLists([[10,15]],[[10,15]])
    
    print "\n\nBaseApplication.getRangeDiff Extensive\n"
    print BaseApplication.getRangeDiff((5,10), (1,2))
    print BaseApplication.getRangeDiff((5,10), (1,5))
    print BaseApplication.getRangeDiff((5,10), (1,7))
    print BaseApplication.getRangeDiff((5,10), (1,10))
    print BaseApplication.getRangeDiff((5,10), (1,15))
    print BaseApplication.getRangeDiff((5,10), (5,7))
    print BaseApplication.getRangeDiff((5,10), (5,10))
    print BaseApplication.getRangeDiff((5,10), (5,15))
    print BaseApplication.getRangeDiff((5,10), (7,8))
    print BaseApplication.getRangeDiff((5,10), (7,10))
    print BaseApplication.getRangeDiff((5,10), (7,15))
    print BaseApplication.getRangeDiff((5,10), (10,15))
    print BaseApplication.getRangeDiff((5,10), (12,15))
    
    print "\n\nBaseApplication.insertRange Extensive\n"
    print BaseApplication.insertRange([[3,7],[10,15]], [1,2]) , "  -----  ",[[1,2],[3,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [1,3]) , "  -----  ",[[1,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [1,4]) , "  -----  ",[[1,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [1,7]) , "  -----  ",[[1,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [1,8]) , "  -----  ",[[1,8],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [1,10]) , "  -----  ",[[1,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [1,12]) , "  -----  ",[[1,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [1,15]) , "  -----  ",[[1,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [1,17]) , "  -----  ",[[1,17]]
    
    print BaseApplication.insertRange([[3,7],[10,15]], [3,4]) , "  -----  ",[[3,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [3,7]) , "  -----  ",[[3,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [3,8]) , "  -----  ",[[3,8],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [3,10]) , "  -----  ",[[3,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [3,12]) , "  -----  ",[[3,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [3,15]) , "  -----  ",[[3,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [3,17]) , "  -----  ",[[3,17]]
    
    print BaseApplication.insertRange([[3,7],[10,15]], [5,6]) , "  -----  ",[[3,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [5,7]) , "  -----  ",[[3,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [5,8]) , "  -----  ",[[3,8],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [5,10]) , "  -----  ",[[3,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [5,12]) , "  -----  ",[[3,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [5,15]) , "  -----  ",[[3,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [5,17]) , "  -----  ",[[3,17]]
    
    print BaseApplication.insertRange([[3,7],[10,15]], [7,8]) , "  -----  ",[[3,8],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [7,10]) , "  -----  ",[[3,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [7,12]) , "  -----  ",[[3,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [7,15]) , "  -----  ",[[3,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [7,17]) , "  -----  ",[[3,17]]
    
    print BaseApplication.insertRange([[3,7],[10,15]], [8,9]) , "  -----  ",[[3,7],[8,9],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [8,10]) , "  -----  ",[[3,7],[8,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [8,12]) , "  -----  ",[[3,7],[8,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [8,15]) , "  -----  ",[[3,7],[8,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [8,17]) , "  -----  ",[[3,7],[8,17]]
    
    print BaseApplication.insertRange([[3,7],[10,15]], [10,11]) , "  -----  ",[[3,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [10,15]) , "  -----  ",[[3,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [10,17]) , "  -----  ",[[3,7],[10,17]]
    
    print BaseApplication.insertRange([[3,7],[10,15]], [12,14]) , "  -----  ",[[3,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [12,15]) , "  -----  ",[[3,7],[10,15]]
    print BaseApplication.insertRange([[3,7],[10,15]], [12,17]) , "  -----  ",[[3,7],[10,17]]
    
    print BaseApplication.insertRange([[3,7],[10,15]], [15,17]) , "  -----  ",[[3,7],[10,17]]

    print BaseApplication.insertRange([[3,7],[10,15]], [17,19]) , "  -----  ",[[3,7],[10,15],[17,19]]

def test5(li1,li2,li3,li4):
    
    userSelectionTempDataStore = baseApplication.userSelectionTempDataStore
    nodeDataStore = baseApplication.nodeDataStore
    listItemDataStore = baseApplication.listItemDataStore
    
    c = li1#ListItem.CreateNew("item1", "some delicious item", "item1", parentListItemRef= None , quantity = 5 )
    d = li2#ListItem.CreateNew("item2", "some delicious item", "item2",parentListItemRef=c.ref, quantity=100 )
    e = li3#ListItem.CreateNew("item3", "some delicious item", "item3",parentListItemRef=c.ref, quantity=100 )
    f = li4#ListItem.CreateNew("item4", "some delicious item", "item4",parentListItemRef=e.ref,quantity=100 )
    

    
    #create the structure 
    #create Paths
    print c.ref , d.ref, e.ref, f.ref
    #create user selection and select something in each node
    a1 = UserSelection.CreateNew(c.ref, None , None, None, None,save = True)
    #log.info(c.childLists)
    childSelections = map(userSelectionTempDataStore.getUserSelectionByRef,a1.onSelection([[0,2],[3,5]]))
    refs = childSelections[1].onSelection([[0,5]])
    print refs
    fItem1 = map(userSelectionTempDataStore.getUserSelectionByRef,refs)[0]   #selection in c 0,2 , e, 0,5
    print childSelections[1].listItemRef , fItem1.listItemRef
    fItem2 = map(userSelectionTempDataStore.getUserSelectionByRef ,childSelections[3].onSelection([[0,5]]))[0]  #selection in c 3,5 , e, 0,5
    
    fItem1.onSelection([[0,99]])
    fItem2.onSelection([[0,1]])
    userSelectionTempDataStore.dfsUserSelectionsAndInsert(a1)

    
    #create user selection and select something in each node
    a1 = UserSelection.CreateNew(c.ref, None , None, None, None, save=True)
    #log.info(c.childLists)
    childSelections = map(userSelectionTempDataStore.getUserSelectionByRef, a1.onSelection([[0,3]]))
    fItem1 = map(userSelectionTempDataStore.getUserSelectionByRef,childSelections[1].onSelection([[0,5]]))[0]   #selection in c 0,3 , e, 0,5
    fItem1.onSelection([[99,100]])
    
    userSelectionTempDataStore.dfsUserSelectionsAndInsert(a1)
    
    # remove from grouping
    u1 = UserSelectionNodes([0,1],[[0,1]],c.ref)
    u2 = UserSelectionNodes([0,5],[[0,5]],e.ref)
    u3 = UserSelectionNodes([0,10],[[0,10]],f.ref)
    
    userSelectionTempDataStore.setAsAvailable(None,userSelectionNodeList=[u1,u2,u3]) #incorrect function have to pass userSelection instead of userSelectionNdoes , just for testing
    
    
    ## check what are available and not available 
    
    un1 = UserSelectionNodes([1,5],[],c.ref)
    un2 = UserSelectionNodes([0,6],[],e.ref)
    un3 = UserSelectionNodes(None,[],f.ref)
    
    baseApplication.getNodeDataStore().findAvailables(None, [un1,un2,un3] , 0,  [], [], [0,5], [])
    #print getNodesBy(None, c.ref, [0,2], 1)
    baseApplication.nodeDataStore.printAllUnavailableNodes()
    
    
#test2()
def test8():
    class A(Document):
        l=ListField(StringField())
        def doSomething(self):
            self.l.remove("world")
    A.drop_collection()
    a = A()
    a.l=["hello","world"]
    a.save()
    
    a1 = A.objects()[0]
    a1.doSomething()
    a1.save()
    print A.objects()[0].l

def test9():
    class B(EmbeddedDocument):
        l=StringField()

    class A(Document):
        p=EmbeddedDocumentField(B)
    
    A.drop_collection()
    a = A()
    c =A()

    a.p= c.p = B(l="do it")
    a.save()
    c.save()
    
    print A.objects()[0].p.l, A.objects()[1].p.l

def test10():

    class A(Document):
        r=EmbeddedDocumentField(Range)
    
    A.drop_collection()
    
    a = A()
    a.r= Range.createRange(10,50)
    a.save()
    
    print A.objects.filter(Q(r__a__not__gt=100) & Q(r__b__not__lt=30))[0].r.a

def test11():
    
    userSelectionTempDataStore = baseApplication.userSelectionTempDataStore
    nodeDataStore = baseApplication.nodeDataStore
    listItemDataStore = baseApplication.listItemDataStore
    #test data
    user_1 = {
                          "ref":"abhinavabcd",
                          "name":"S.AbhinavReddy",
                          "phoneNumber":"+917680971071",
                          "listitems" :[],
                          "properties" :{},
                          "menu" :  ""                   
    }
    bo = User(**user_1)
    bo.save()
    listItem_1 = {
    "_rangeList": [[0,5]],
    "parentListItemRef" : None, 
    "rootListItemRef" : None,
    "parentPath" : None,
    "childLists" : [],
    "linkedLists" :[], # linked ListItems , they are not childs of this list nor consuming these will destroy the parent  
    
    "name": "Vincent Chase VC 4323 Matte Black White 1110 Eyeglasses ",#required=True)# this is unique for a business operator for all his items accross , and users this name in the api #TODO if required , change to bo_given_name
    "description" : '''
                <h3>Product description</h3>
                <p style="width:100%;word-wrap:break-word;"><span itemprop="description"></span></p><p>Get ready to impress your beholders with this modish pair of specs from <a href="/eyeglasses/brands/vincent-chase-eyeglasses.html">Vincent Chase</a>. Crafted in a sleek and eye-catching design, these glasses are appropriate for both men and women. The rectangular, full-rimmed style of glasses is an all time favourite. The stainless steel frame makes this frame sturdy and comfortable.</p>
<p><strong>Features</strong></p>
<ul>
<li>Durable and light-weight stainless steel frame</li>
<li>1 year warranty for manufacturing defect</li>
<li>Medium frame size</li>
<li>Bifocal prescription type</li>
<li>Corrosion resistant temple material</li>
</ul>
<p>Sport these black and white specs at work and be noticed for your impeccable sense of style. <a href="/">Lenskart</a> offers you a flat 50% discount on this smart eyewear!</p>
<br><br>
<img style="display: inline;" class="lazy" alt="Lenskart" src="http://d37l6i9uhpr090.cloudfront.net/media/wysiwyg/blanco/mix-theme-12-3-14/D_4387.jpg" data-original="http://d37l6i9uhpr090.cloudfront.net/media/wysiwyg/blanco/mix-theme-12-3-14/D_4387.jpg" width="100%" border="0"><br><br>
<img style="display: inline;" class="lazy" alt="Lenskart" src="http://d37l6i9uhpr090.cloudfront.net/media/wysiwyg/blanco/mix-theme-12-3-14/D_4388.jpg" data-original="http://d37l6i9uhpr090.cloudfront.net/media/wysiwyg/blanco/mix-theme-12-3-14/D_4388.jpg" width="100%" border="0"><br><br>
<img style="display: inline;" class="lazy" alt="Lenskart" src="http://d37l6i9uhpr090.cloudfront.net/media/wysiwyg/blanco/mix-theme-12-3-14/M_1416.jpg" data-original="http://d37l6i9uhpr090.cloudfront.net/media/wysiwyg/blanco/mix-theme-12-3-14/M_1416.jpg" width="100%" border="0"><br><br>
<img style="display: inline;" class="lazy" alt="Lenskart" src="http://d37l6i9uhpr090.cloudfront.net/media/wysiwyg/blanco/mix-theme-12-3-14/M_1417.jpg" data-original="http://d37l6i9uhpr090.cloudfront.net/media/wysiwyg/blanco/mix-theme-12-3-14/M_1417.jpg" width="100%" border="0"><p></p>
            ''',
    "images" : ["http://d3kw6wfam8lfab.cloudfront.net/media/catalog/product/cache/1/thumbnail/40x40/9df78eab33525d08d6e5fb8d27136e95/D/_/D_9854_1.jpg","http://d3kw6wfam8lfab.cloudfront.net/media/catalog/product/cache/1/thumbnail/40x40/9df78eab33525d08d6e5fb8d27136e95/D/_/D_9856.jpg"],
    "discreteFactor" : 1,
    "isContinuous" : False,
    "isUnique" : False,
    "unitPrice" : 500 ,
    "unitFakePrice" : 800,
    "isUserAuth" : True,
    "isTime" : False,
    "ownerUser" : "user1",
    "boUid" : "VC0001",# to use in business opearator api
    "group_Type" : "Samsung Galaxy S5",
    "selectionType" : SELECT_ONE,
    "selectionMinMax" : [1,3],
    "viewSpan" : 10,
    "isNeeded" : True,
    "expandForEach" : False,
    "isEndPoint" : False,
    "uiRenderRef" : "quantitySelect",
    "searchTags" : ["EYE_GLASSES"],
    "isSearchNode" : False,
    "indexToItemMap" : [ [[0,1],"A1",300],[[1,2],"A2",400] ],
    "discountConditions" : [offerSnippets.CreateNew("Buy two kgs and get a discount of 2 Rs","this.price = this.price - this.getQuantity()/2 *2")],
    "afterCartConditions" : [offerSnippets.CreateNew("Buy two kgs and get a discount of 2 Rs","this.price = this.price - this.getQuantity()/2 *2")],
    "isApprovalNeeded" : False,
    "properties" : {},
    "commonUserTextInput" : [["fresh","how much fresh do you want ?"],["colorContrast", "mention a color contrast you want ?","20"]],
    "commonUserFileInput" : [["printableFile1"]],
    "commonUserSelectInput" : [["selectLetter" , ['a','b','c'], [10,20,30]],["selectSomething[]",["dumber1","dumber2","dumber3"],[10,30,400]] ],
    "commonUserRadioInput" : [["fresh","how much fresh do you want ?","val*5"],["colorContrast", "mention a color contrast you want ?","20"]],
    
    "userTextInput" :[["fresh","how much fresh do you want ?"],["colorContrast", "mention a color contrast you want ?",20]],
    "userFileInput" : [["printableFile1"]],
    "userSelectInput" : [["selectLetter" , ['a','b','c'], ["10*quantity",20,30]],["selectSomething[]",["dumber1","dumber2","dumber3"],[10,30,400]] ],
    "userRadioInput" : [["fresh","how much fresh do you want ?","quantity*5"],["colorContrast", "mention a color contrast you want ?","20"]]
    }
    
    liobj1 = ListItem.CreateNew(save=True,**listItem_1)
    p1 =  Path.CreateNew(liobj1.ref, "")
    
    
    listJson2 = {'parentPath':p1,
                 "_rangeList": [[0,100]], 
                 "name" : "Jack Sparrow 420 Matte Black White 1110 Eyeglasses ",
                 "parentListItemRef" : liobj1.ref,
                "save":True 
                 }
    
    listItem_1.update(listJson2)
    liobj2 = ListItem.CreateNew(**listItem_1)

    p2 = Path.CreateNew(liobj1.ref, "")
    
    listJson3 = {'parentPath':p2,
                 "_rangeList": [[0,100]], 
                 "name" : "Will Smith 007 Pure Black 1110 Eyeglasses ",
                 "parentListItemRef" : liobj1.ref,
                "save":True 
                 }
    
    listItem_1.update(listJson3)
    liobj3 = ListItem.CreateNew(**listItem_1)

    p3 = Path.CreateNew(liobj1.ref, "")
    
    listJson4 = {'parentPath':p3,
                 "_rangeList": [[0,100]], 
                 "name" : "Black Smith 007 Pure Black 1110 Eyeglasses 111",
                 "parentListItemRef" : liobj3.ref,
                "save":True 
                 }
    
    listItem_1.update(listJson4)
    liobj6 = ListItem.CreateNew(**listItem_1)


    liobj4 = ListItem.CreateNew(**listItem_1)
    liobj4.setRangeList([[10,20]])
    liobj4.setName("Samosa 000 Food")
    liobj4.save()
    
    liobj5 = ListItem.CreateNew(**listItem_1)
    liobj5.setRangeList([[0,20]])
    liobj5.setName("PopCorn 000 Food ")
    liobj5.save()
    
    liobj2.linkedLists.append(liobj4.ref)
    liobj3.linkedLists.append(liobj5.ref)
    
    return liobj1,liobj2,liobj3,liobj4,liobj5,liobj6

def test12():
    # test listitem class and listitemdatastore
    liobj1,liobj2,liobj3,liobj4,liobj5,liobj6 = test11()
    test5(liobj1,liobj2,liobj3,liobj6)
    listitemobj = ListItem.objects[0]
    print listitemobj.getName()
    listitemref = ListItem.objects[0].getChildLists()[0]
    print baseApplication.listItemDataStore.getListItemByRef(listitemref).getName() #listitemobj.getName()
    
def test13():
    # test Base Application
    baseApplication = BaseApplication()
    
if __name__ == '__main__':
#    cProfile.run("test12()","from __main__ import *")#.timeit()
    test4()