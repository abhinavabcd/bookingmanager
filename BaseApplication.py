'''
Created on May 2, 2014

@author: abhinav2
'''


from mongoengine import *
from Config import *
import json
import time
import datetime 
from sets import Set
from  collections import deque
from threading import Lock
import bson
import itertools
from datetime import date
from BmpImagePlugin import SAVE
db = connect('bizup')
#db.drop_database('bizup')


class Counter(Document):
    count = IntField()


if(not len(Counter.objects())):
    Counter(count=0).save()
    
def getUniqueId():
    global N_PROCESSES,PROCESSES_ID
#    if __count == 0:
    countObj = Counter.objects().get(0)
#        __count = max(ListItem.count(), Node.count(), UserSelection.count(), UserSelectionMap.count(), Menu.count(), Item.count(), Path.count())
#        print "count changed"
    countObj.count+=1
    countObj.save()
    return str(int(N_PROCESSES*countObj.count + PROCESSES_ID))

    
#    def getUniqueId(self):
#        self.current += 1
#        self.save()
#        return self.current


class Menu(Document):
    ref = StringField()
    menuJson = StringField()
    
    @staticmethod
    def CreateNew(menuJson):
        self = Menu()
        self.ref = getUniqueId()
        self.menuJson = menuJson
        self.save()
        return self
    
        
class Group(Document):    
    ref= StringField()
    menu = ReferenceField(Menu)
    #TODO:
    
class User(Document):
    ref= StringField(max_length=20 , required= True,unique=True)
    name = StringField()
    phoneNumber = StringField()    
    properties = DictField()
    listItems = ListField(ReferenceField('ListItem'))
    menu = ReferenceField('Menu')
    @staticmethod
    def CreateNew(name,phno,properties,listitems,menu,save=False):
        self = User()
        self.ref = getUniqueId()
        self.name = name
        self.phoneNumber = phno
        self.properties = properties
        self.listItems = listitems
        self.menu = menu
        if(save):
            self.save()
        return self
        
    '''api function'''
    def getPurChasedListItemsFromBo(self):
        '''TODO:'''
        pass
    def removeListItem(self,listItemRef):
        self.objects.update_one(pull__listItems__ref=listItemRef)

    #TODO later , remove one field later 
    #listItemRefs =ListField(StringField()) # all services he creates added to List
        

class Transaction(Document):
    ref= StringField(max_length=20 , required= True,unique=True)
    data = StringField()#string
    amount = IntField()
    
    # BO can revert transactions when needed (Eg:guitar tabs request which he cannot create)
    '''TODO:'''
    def revertTransaction(self):
        pass
    
class UiRenderer(Document):
    data = StringField()

'''decorator'''
saveInDbMap ={}
def delaySaveInDb(self):
    x = id(self)
    n = saveInDbMap.get(x,None)
    if(n ==None): saveInDbMap[x]=1 
    else: saveInDbMap[x] = n+1 


def trySaveInDb(self):
    x = id(self)
    n = saveInDbMap.get(x,0)

    saveInDbMap[x]=n-1
    if(n-1<=0): 
#        print "B1"    
        self.save() 
        del saveInDbMap[x]
    

    

def saveInDb(func):
    def wrapped(self, *args, **kw):
        delaySaveInDb(self)
             
        ret = func(self, *args, **kw)
 
        trySaveInDb(self) 
        return ret
    return wrapped

# 
# def saveInDb(func):
#     def wrapped(self, *args, **kw):
#             
#         ret = func(self, *args, **kw)
# 
#         dirtySave = self.getDirtyObjects()
#         while(dirtySave):
#             dirtySave.pop().save()
# 
#         return ret
#     return wrapped


def saveInDbNow(func):
    def wrapped(self, *args, **kw):
        ret = func(self, *args, **kw)
        self.save()
        return ret
    return wrapped

'''decorator'''
def commitDirty(func):
    def wrapped(self, *args, **kw):
        baseApplication.commitQueueToDb()
        return  func(self, *args, **kw)
    return wrapped
'''decorator'''
def bulkDbUpdate(func):
    def wrapped(self, *args, **kw):
        baseApplication.dbUpdateFlag = False
        ret = func(self, *args, **kw)
        baseApplication.dbUpdateFlag = True
        baseApplication.commitQueueToDb()

        return ret
    return wrapped
#This is the produced by the list Item 
class Item(Document):
    ref= StringField(max_length=50 ,required=True,unique=True)
    index = IntField() # IMP:  item should have an index definitely defined to be valid
    quantity = IntField()
    name = StringField(max_length=100) #,required=True,unique=True)
    price = IntField(default = 0)
    #list of jsons
    logs = ListField(StringField())
    listItemRef = StringField()
    properties = DictField()
    #additional string properties
    userTextInput = ListField(StringField())
    userFileInput = ListField(StringField())
    
    pendingProperties = DictField()
    
    @staticmethod
    def CreateNew(index=0,quantity=0,listItem = None , save=True): 
        self = Item()
        self.ref = getUniqueId()
        self.index= index
        self.quantity = quantity
        self.listItemRef = listItem.ref
        self.properties={"isApprovalNeeded":False}
        if(save):
            self.save()
        return self
    @saveInDb
    def addLog(self, log):
        self.logs.append(log)
        
    def getDates(self):#API kind of function
        i = 0
        l=[]
        while(True):
            if(i*SECONDS_IN_DAY>self.quantity):
                return l 
            l.append(datetime.datetime.fromtimestamp(int(self.index+i*SECONDS_IN_DAY)))
            i+=1
        return l
    
    @saveInDb
    def updateApproval(self,boolean):
        self.isApprovalNeeded = True
    
    def getDate(self):#APi function
        return datetime.datetime.fromtimestamp(int(self.index))

    @saveInDb
    def incrementPrice(self,string):#API function
        #TODO this
        pass
    def getQuantity(self):
        return self.quantity
    
    @saveInDb
    def setPrice(self,price,pyCode=""):#API function
        #TODO :: implement a user exec function here
        '''
        if(getDate().weekday() == THURSDAY):price = price - 20  ,, api kind of 
        '''
        this = self
#        print pyCode
        exec compile(pyCode,'<string>','exec')#modify price variable in this
        if price:
            self.price = price
    
    @saveInDb
    def updateprice(self,price):
        self.price = price
    
    def getPrice(self):
        return self.price
        
    @saveInDb
    def setBookingId(self,bookingId):
        self.bookingId = bookingId
    
    def getRange(self):
        return [self.index,self.index+self.quantity]
    
    @saveInDb
    def setProperty(self,name,value):
        self.properties[name] =value
        
    def getProperty(self,name, defaultValue):
        if(name in self.properties):
            return self.properties[name]
        
        listItem = baseApplication.getListItemDataStore().getListItemByRef(self.listItemRef)
        if(name in listItem._data):
            return listItem._data[name]
        
        return defaultValue
        
    @saveInDb
    def setRange(self, r):
        self.index = r[0]
        self.quantity = r[1]-r[0]
        
    def setHTMLDisplay(self,html):
        pass
    
    @saveInDbNow
    def afterpayment(self):
        for i in self.pendingProperties.keys():
            self.setProperty(i, self.pendingProperties[i])
        del self.pendingProperties

'''
This class is defined for use of the Ui design , this is like , for each group of items , we shall load script 
Keeping it simple  , will have just one group id for listItem and max a script can be used.
'''
class UiScriptsByGroupId(Document):
    boGroupId = StringField(required= True)
    scriptFile = StringField(required= True)
    
    
class offerSnippets(EmbeddedDocument):
    description = StringField()
    pyCode = StringField()
    
    @staticmethod
    def CreateNew(desc=None,pycode=None):
        self = offerSnippets()
        self.description = desc
        self.pyCode = pycode
        return self
        
    def getPyCode(self):
        return self.pyCode
    
    def setPyCode(self,pycode):
        self.pyCode = pycode
        
    def getDescription(self):
        return self.description
    
    def setDescription(self,desc):
        self.description = desc
    
class Path(EmbeddedDocument):
    
    parentListItemRef = StringField(required=True)
    listItemRef = StringField(required=True)
    pyCode = StringField(required=True)
    payload = StringField(default="")
    def isValidPath(self,item):
        valid = 1 
        #0 means invlid , ! means valid 
        #2 means , invalid but show for user for selection just like a side thing that can be selected but not considered in removing the parent item when the items from this listItemRef are consumed
        exec compile(self.pyCode, '<string>', 'exec')
        return valid    
    @staticmethod
    def CreateNew( parentListRef , pyCode="",payload = None):
        self = Path()
        self.ref = getUniqueId()
        self.parentListItemRef = parentListRef
        self.pyCode = pyCode
        self.payload = payload
        #save in db
        #self.save()
        return self
    def setListItem(self,listItem):
        self.listItemRef = listItem.ref
        return self
    
    def __eq__(self,path):
        return path.ref == self.ref
        
#     def clone(self):
#         path = Path(self.parentListItemRef, self.listItemRef , self.evalStr,self.payload)
#         path.ref = self.ref
#         return path


    
''''
Define groups pertaining to business operator and search capability in general for all business opeartors
TODO:
'''
class ListItem(Document):
    ####properties
    #list variables
    #each entry in range list is definitely exclusive
    ref = StringField(max_length=50 ,required=True,unique=True)
    _rangeList=ListField(ListField(IntField(),required=True))
    parentListItemRef = StringField() 
    rootListItemRef = StringField()
    parentPath = EmbeddedDocumentField('Path')
    childLists = ListField(StringField()) #inner elements by reference    
    linkedLists = ListField(StringField()) # linked ListItems , they are not childs of this list nor consumind these will destroy the parent  
    
    name = StringField(required=True,unique=True)#required=True)# this is unique for a business operator for all his items accross , and users this name in the api #TODO if required , change to bo_given_name
    description = StringField(required=True)
    images = ListField(StringField(),required=True)
    discreteFactor = IntField(default=1,required=True)
    isContinuous = BooleanField(default=False,required=True)
    isUnique = BooleanField(default=False,required=True)
    unitPrice = IntField(default = 0,required=True)
    unitFakePrice = IntField(default=0, required=True) 
    isUserAuth = BooleanField(default=True) #only of authenticated users
    isTime = BooleanField(default = False)
    ownerUser= ReferenceField(User, required=True) # reference to business operator
    boUid = StringField() # items bo_groupName , #logical grouping of set of listItems , for later use like custom js scripts for a set of listItems
    group_Type = StringField() # 
    selectionType = IntField(default=SELECT_ONE,required=True) #, SELECT_RANGE 
    selectionMinMax= ListField(default=[1,1],required=True)
    viewSpan = IntField(default=10,required=True) #range to view while displaying the ui decided by business operator
    isNeeded = BooleanField(default=True,required=True) # at this level of the graph the selection is compulsory    
    expandForEach = BooleanField(default=True,required=True)#for each discrete quantity
    isEndPoint = BooleanField(default=False,required=True)
    uiRenderRef = StringField(required=True)
    
    searchTags = ListField(StringField(required=True))
    isSearchNode = BooleanField(default=False,required=True)
    
    indexToItemMap = ListField(ListField()) #json array of arrays [[indexRange, nameExpression, price, group, ]] 
    discountConditions = ListField(EmbeddedDocumentField(offerSnippets))#array of jsons , [name,pycode] ...]
    afterCartConditions = ListField(EmbeddedDocumentField(offerSnippets))#array of jsons , [name,pycode] ...]
    
    listItemHeadRef = StringField() # can be used to search for items
    isApprovalNeeded = BooleanField(required =True , default = False)
    '''initial input properties that needs to be set to the item set to the item in item.properties and update item price accordingly'''
    properties = DictField()
    commonUserTextInput = ListField(ListField())#Note: json array of items in the stringfield
    commonUserFileInput = ListField(ListField())
    commonUserSelectInput = ListField(ListField())
    commonUserRadioInput = ListField(ListField())
    
    userTextInput = ListField(ListField()) #unique text input field for each item from common list
    userFileInput = ListField(ListField())#for each item
    userSelectInput = ListField(ListField())
    userRadioInput = ListField(ListField())
    
   # made the arguments linedup just for clear reading
    @staticmethod
    def CreateNew(quantity=None,_rangeList = None,parentListItemRef = None,rootListItemRef = None,parentPath=None,childLists = [],linkedLists = [],name = None,description = None,images = None,discreteFactor = 1,isContinuous = False,isUnique = False,unitPrice = None,unitFakePrice = None,isUserAuth = True,isTime=False,ownerUser = None, boUid = None,group_Type = None,selectionType = None,selectionMinMax = None,viewSpan = None,isNeeded = None,expandForEach = None,isEndPoint = None,uiRenderRef = None,searchTags = None,isSearchNode = None,indexToItemMap = None,discountConditions = None,afterCartConditions = None,isApprovalNeeded = False,properties=None,commonUserTextInput = [],commonUserFileInput = [],commonUserSelectInput = [],commonUserRadioInput = [],userTextInput = [],userFileInput = [],userSelectInput = [],userRadioInput = [],save = True):
        self = ListItem()
        delaySaveInDb(self)

        self.ref = getUniqueId() #like the object reference
                     
        if(quantity== -1 or quantity == None):
            quantity = INT_MAX
            
        if(_rangeList):
            self.setRangeList(_rangeList)
        else:
            self.setRangeList([[0,quantity]])
            
        self.parentListItemRef = parentListItemRef
        if(parentListItemRef):
            parentListItem = baseApplication.getListItemDataStore().getListItemByRef(parentListItemRef)
            parentListItem.addChildLists(self)
             
        if(not rootListItemRef):
            parentRef = self.parentListItemRef
            while(parentRef!=None ):
                parentItem = baseApplication.getListItemDataStore().getListItemByRef(parentRef)
                self.rootListItemRef = parentItem.rootListItemRef
                if (parentItem.rootListItemRef):
                    break
                else:
                    self.rootListItemRef = parentRef
                    parentRef = parentItem.parentListItemRef
        else:
            self.rootListItemRef = rootListItemRef
            
        if(parentPath):
            self.parentPath = parentPath.setListItem(self)
            
        self.childLists=childLists #inner elements by reference
        self.linkedLists=linkedLists
        
        self.setName(name)
        self.setDescription(description)
        self.addImages(images)
        self.setDiscretizationFactor(discreteFactor)
        self.setIsContinuous(isContinuous)
        self.setIsUnique(isUnique)
        self.setUnitPrice(unitPrice)
        self.setUnitFakePrice(unitFakePrice)
        self.setIsUserAuth(isUserAuth)
        self.isTime = isTime
        self.ownerUser = ownerUser
        self.setBoUid(boUid)
        self.setGroupTypeId(group_Type)
        self.setSelectionMinMax(selectionMinMax)
        self.setViewSpan(viewSpan)
        self.setIsNeeded(isNeeded)
        
        self.expandForEach = expandForEach
        if(isContinuous):
            self.expandForEach = False
        self.isEndPoint = isEndPoint

        self.uiRenderRef = uiRenderRef
        self.setSearchTags(searchTags)
        if(isSearchNode):
            self.setIsSearchNode(isSearchNode)
#            self._rangeList=[[0,1]]
        '''this indicates the selection not less than the index , for inserting/editing'''
#        self.freezeIndex = freezeIndex 
        if(indexToItemMap):
            self.setIndexToItemMap(indexToItemMap)#json
            self.expandForEach = True
            self.isUnique = True
            self.isContinuous = False
        
        if(discountConditions):
            self.discountConditions = discountConditions
        else:
            self.discountConditions = []    
        self.afterCartConditions = afterCartConditions
        self.isApprovalNeeded = isApprovalNeeded #intial falg to indicate an approval is needed
        self.properties = properties
        self.userTextInput = userTextInput   #[[name,description,etc]]     #unique text input field for each item from common list
        self.userFileInput = userFileInput #[ [name, description , etc] ]
        self.userSelectInput = userSelectInput 
        self.userRadioInput = userRadioInput
        
        self.commonUserTextInput = commonUserTextInput
        self.commonUserFileInput = commonUserFileInput
        self.commonUserSelectInput = commonUserSelectInput
        self.commonUserRadioInput = commonUserRadioInput
#         
#         # for each item user owns , the business operator has to submit the following peoperties
#         self.boTextInput = []
#         self.boFileInput=[]
#         self.boSelectInput =[]
#         self.boRadioInput = []
        #save in db

        if(save):
            self.save()
        trySaveInDb(self)

        return self
    
    
    def isTimeRange(self):
        return self.isTime
    
    
    # getters setters are in order from here
    
    def getName(self):
        return self.name
    
    def setName(self,name):
        self.name = name
        
    def getRef(self):
        return self.ref
    
    @saveInDb
    def setRangeList(self, rlist):
        self._rangeList = rlist
    
    def getRangeList(self):
        return self._rangeList
                    
    def getParentPath(self):
        return self.parentPath
        
    def getChildListRefs(self):
        return self.childLists
    
    @saveInDb
    def addChildLists(self,childListItem):
        self.getChildListRefs().append(childListItem.ref)
    
    @saveInDb
    def removeChildLists(self,childListItemRef):
        if(childListItemRef in self.getChildListRefs()):
            self.getChildListRefs().remove(childListItemRef)
            
    def getDescription(self):
        return self.description

    def setDescription(self,description):
        self.description = description

    def getImages(self):
        return self.images
    
    @saveInDb
    def addImages(self,images):
        for img in images:
            if self.images.count(img)==0:
                self.images.append(img)
    
    @saveInDb    
    def removeImages(self,images):
        for img in images:
            if self.images.count(img)>0:
                self.images.remove(img)
                
    def getDiscretizationFactor(self):
        return self.discreteFactor
    
    @saveInDb
    def setDiscretizationFactor(self, val):
        self.discreteFactor = val    
    
    def getIsContinuous(self):
        return self.isContinuous
    
    @saveInDb
    def setIsContinuous(self, boolval):
        self.isContinuous = boolval

    @saveInDb
    def setGroupTypeId(self , typeId):
        self.group_Type = typeId
        
        
    def getIsUnique(self):
        return self.isUnique
    
    @saveInDb
    def setIsUnique(self, boolval):
        self.isUnique = boolval
        
    def getUnitPrice(self):
        return self.unitPrice
    
    @saveInDb
    def setUnitPrice(self,val):
        self.unitPrice = val    
        
    def getUnitFakePrice(self):
        return self.unitFakePrice
    
    @saveInDb
    def setUnitFakePrice(self,val):
        self.unitFakePrice = val
        
    def getIsUserAuth(self):
        return  self.isUserAuth
    
    @saveInDb
    def setIsUserAuth(self, boolval):
        self.isUserAuth = boolval
        
    def getBoUid(self):
        return self.boUid
    
    @saveInDb
    def setBoUid(self,groupId):
        #TODO: check ,if group id already exists; currently using just string - need to implement users
        self.boUid = self.getOwnerUser().ref+"_"+groupId
        
    def getSelectionType(self):
        return self.selectionType
    
    @saveInDb
    def setSelectionType(self,seltype):
        self.selectionType = seltype
        
    def getSelectionMinMax(self):
        return self.selectionMinMax
    
    @saveInDb
    def setSelectionMinMax(self,minmax):
        self.selectionMinMax = minmax
        
    def getViewSpan(self):
        return self.viewSpan
    
    @saveInDb
    def setViewSpan(self, val):
        self.viewSpan = val
        
    def getIsNeeded(self):
        return self.isNeeded
    
    @saveInDb
    def setIsNeeded(self, boolval):
        self.isNeeded = boolval

    def getIsExpandForEach(self):
        return self.expandForEach
    
    @saveInDb
    def setIsExpandForEach(self, boolval):
        self.expandForEach = boolval
    
    def getIsEndPoint(self):
        return self.isEndPoint
    
    @saveInDb
    def setIsEndPoint(self, boolval):
        self.isEndPoint = boolval
    
    def getOwnerUser(self):
        return self.ownerUser
    
    def getUiRenderRef(self):
        return self.uiRenderRef
    
    @saveInDb
    def setUiRenderRef(self, ref):
        self.uiRenderRef = ref
    
    def getSearchTags(self):
        return self.searchTags
    
    @saveInDb
    def setSearchTags(self,tagList):
        tags = []
        for L in range(1, len(tagList)+1):
            for subset in itertools.combinations(tagList, L):
                tags.append("_".join(sorted(subset)))
        self.searchTags = tags
    
    def getIsSearchNode(self):
        return self.isSearchNode
    
    @saveInDb
    def setIsSearchNode(self, boolval):
        self.isSearchNode = boolval
        
    def getIndexToItemMap(self):
        return self.indexToItemMap
    
    @saveInDb
    def setIndexToItemMap(self,itemmap):
        #If index to item map is given as single item index it converted to Range as per our notation
        for imap in itemmap:
            if (isinstance(imap[0],list)):
                if (len(imap[0]) == 1):
                    imap[0] = [imap[0][0],imap[0][0]+1]
            else:
                imap[0] = [imap[0],imap[0]+1]
        self.indexToItemMap = itemmap
                
    def getDiscountConditions(self):
        return self.discountConditions
    
    @saveInDb
    def addDiscountConditions(self,snippets):
        for snippet in snippets:
            self.discountConditions.append(snippet)
        
    @saveInDb        
    def removeDiscountConditionsByIndex(self,indList):
        if self.discountConditions:
            for ind in indList:
                del self.discountConditions[ind]
    
    def getAfterCartConditions(self):
        return self.afterCartConditions
    
    @saveInDb
    def addAfterCartConditions(self,snippets):
        for snippet in snippets:
            self.afterCartConditions.append(snippet)
        
    @saveInDb        
    def removeAfterCartConditionsByIndex(self,indList):
        if self.afterCartConditions :
            for ind in indList:
                del self.afterCartConditions[ind]            
    
    def getPropertyByKey(self,key):
        if self.properties.has_key(key):
            return self.properties(key)
    
    @saveInDb
    def setPropertyByKey(self,key,val):
        self.properties[key] = val
        
    @saveInDb
    def removePropertyByKey(self,key):
        if self.properties.has_key(key):
            del self.properties[key]
     
    '''TODO: Complete Getters Setters for all inputs'''    
    def getCommonUserTextInput(self):
        return self.commonUserTextInput
    
    @saveInDb
    def addCommonUserTextInput(self, inp=None, desc=None, priceFunc=None):
        self.commonUserTextInput.append([inp,desc,priceFunc])
    
    @saveInDb
    def setUserTextInputs(self):
        pass
      
# getters setters are in order till here             
        
    def getItemByIndex(self,index,quantity,save=False):
        #TODO : set item properties more
        item = Item.CreateNew(index, quantity, self ,save=False)            
        price = self.unitPrice*quantity/self.getDiscretizationFactor()
        item.name = self.name
        item.setPrice(price)
        if(self.indexToItemMap): 
            arr = self.indexToItemMap # earlier json.loads was used
#            if(arr):
            itemProperties = arr[BaseApplication.binary_search_range(arr, index,index+quantity,key=lambda x: x[0])]
            name  = itemProperties[1]
#                exec(name)
            item.name = name
            price = itemProperties[2] if len(itemProperties)>2 else price
            item.setPrice(price)
        
        for offerSnippet in self.getDiscountConditions():
            discountLogString , code =  offerSnippet.getDescription() , offerSnippet.getPyCode()
            price = item.getPrice()
            item.setPrice(price,code)
            discount = item.getPrice()- price
            if(discount!=0):#either increase or decrease
                item.addLog("["+discountLogString+","+str(discount)+"]")
            '''discount conditions here?'''
        #TODO
        if(save):
            item.save()
        return item
        
    def getPartialRange(self,a,b):#TODO : optimize
        ret= []
        start = False
        for c,d  in self._rangeList:
            if(c<=a and a < d):
                ret.append([a, d if d>b else b])
                start = True
                continue
            if(c <=b and b<=d):
                ret.append(c,b)
                start = False
                break 
            if(start):
                ret.append([c,d])
        return ret
                
    def isValidSelection(self, c , d=None):
        if(not d):
            d= c + self.getDiscretizationFactor()
        rangeList = self._rangeList
        for i in rangeList:
            a = i[0]
            b = i[1]
            if(c>=a and d <=b):
                return True            
        return False
    
    def isEmpty(self):
        for i in self._rangeList:
            if(i[1]-i[0]>0):
                return False
        return True
        
#     def consumeItems(self,itemStart = None, quantity = None ):
#          
#         if(not self.isContinuous()):
#             if(itemStart%self.getDiscretizationFactor() == 0 and quantity%self.getDiscretizationFactor()==0):
#                 pass
#             else:
#                 return None
#              
#         if(not self.isUnique()):
#             if(self._rangeList):
#                 itemStart = self._rangeList[0][0]
#          
#         # every item part is isUnique in this list , so we have to consider both itemStart , quantity
#                  
#         removedItems = self.fragmentList(self._rangeList, itemStart,itemStart+quantity)
#         log.info(removedItems)
#         return removedItems
#     

       
    def getItemsByRange(self, a , b,save=False):
        selectedItems = []

            
        if(not self.getIsContinuous()  and self.getIsExpandForEach()):#discreteFactor and expand for each
            for i in range(a,b,self.getDiscretizationFactor()):
                selectedItems.append(self.getItemByIndex(i, self.getDiscretizationFactor(),save))
                
        else: #discreteFactor but dont expand or continuous
            selectedItems.append(self.getItemByIndex(a, b-a))

                
        return selectedItems
    

    
# 
# class StringProperty(str):
#     default = ""
#     editable = False
#     required = False
#     def getValue(self):
#         return str(self)
#     def setValue(self):
#         pass
#     
#     val = property(getValue,setValue)
#     
#     def __init__(self, *args, **kwargs):
#         str.__init__(self, *args, **kwargs)
#     def __eq__(self, *args, **kwargs):
#         self.val = self.args[0]
#         return str.__eq__(self, *args, **kwargs)   
#     required = False    

class BOPayments(Document):
    ref = StringField(required=True)
    paymentTimestamp = DateTimeField()
    transactionList  = ListField(Transaction)
    userSelectionMap = ReferenceField('UserSelectionMap')
    
    def getTotalTransactionAmount(self):
        total = 0
        for transaction in self.transactionList:
            total = total + transaction.amount
        return total
            

class genericPayLoad():
    name = None
    payload1 = None
    payload2 = None
    def __init__(self,name, payload1 , payload2):
        self.name =  name
        self.payload1 = payload1
        self.payload2 = payload2
    

    
class Range(EmbeddedDocument):
    a = IntField()
    b = IntField()
    @staticmethod
    def createRange(a,b):
        r = Range()
        r.a = a
        r.b = b
        return r
    
class Node(Document): # db storage of our unavailables
    ref=StringField(required=True)
    listItemRef = StringField(required=True)
    rangeVal=EmbeddedDocumentField(Range)#leading __ will rename it to a different name , so , we can conside this as a private variable , you cant refer this variable outside this class with node.rangeVal
    parentNodeRef = StringField()
    payload = None # this filed we shall use to save a range List    
    childNodes=ListField(StringField())#by reference
    filledChildLists=ListField(StringField())
    level = IntField(required=True)
    @staticmethod
    def CreateNew( listItemRef, r, parentNodeRef,level):
        self = Node()
        self.ref = getUniqueId()
        self.listItemRef = listItemRef
        self.payLoad = None 
        self.parentNodeRef = parentNodeRef
        self.childNodes=[]
        self.filledChildLists=[]
        self.level = level
        self.rangeVal = Range.createRange(*r) 
        self.save()
        return self
    def getChildNodesBy(self,listRef,r):
        pass #write this function, user this parent ref and find the node 

    @saveInDb
    def setRange(self,r):
        if(self.rangeVal.a , self.rangeVal.b != r[0] , r[1]):
            self.rangeVal = Range.createRange(*r)
        #save()
    def getRange(self):
        return [self.rangeVal.a , self.rangeVal.b]
    
    def getItems(self):
        return baseApplication.listItemDataStore.getListItemByRef(self.listItemRef).getItemsByRage(*self.getRange())
    
    def __repr__(self):
        return super(Node, self).__repr__()+" "+json.dumps([self.parentNodeRef ,  self.ref, json.dumps(self.getRange()) , self.getChildNodes()])
    
    def getChildNodes(self):
        return self.childNodes
    
    @saveInDb
    def appendChildNodes(self,childNodeRef):
        self.childNodes.append(childNodeRef)
    
    @saveInDb
    def removeChildNodes(self,childNodeRef):
        self.childNodes.remove(childNodeRef)
    
    @saveInDb
    def addFilledChildLists(self,childListItemRef):
        self.filledChildLists.append(childListItemRef)
        #save()
    @saveInDb
    def removeFilledChildLists(self,childListItemRef):
        if(self.checkAndGetFilledChildListitems(childListItemRef)):
            self.filledChildLists.remove(childListItemRef)
    
    @saveInDb
    def checkAndGetFilledChildListitems(self,listItemRef=None):
        if(listItemRef==None):
            return self.filledChildLists
        else:
            return listItemRef if listItemRef in self.filledChildLists else None
        
'''this is temporary and is not saved or a mongo document'''
class UserSelectionNodes():
    _range=None
    selectedRanges =None
    listItemRef =None     
    def __init__(self,_range, selectedRanges ,listItemRef):
        self._range = _range
        self.selectedRanges = selectedRanges
        self.listItemRef = listItemRef
    
    def getSelectedRanges(self):
        return self.selectedRanges if self.selectedRanges else [] 
    
    def setRange(self,r):
        self._range = r
        
    def getRange(self):
        return self._range
    
    @staticmethod
    def fromUserSelections(userSelectionList):
        selectionNodes=[]
        for i in range(len(userSelectionList)-1):
            selectionNodes.append( UserSelectionNodes(userSelectionList[i+1].getParentRange(), userSelectionList[i].selectedRanges , userSelectionList[i].listItemRef))
        
        selectionNodes.append(UserSelectionNodes(None, userSelectionList[i+1].selectedRanges , userSelectionList[i+1].listItemRef))
        return selectionNodes
    
    @staticmethod
    def fromNodes(nodesList):
        selectionNodes=[]
        for i in range(len(nodesList)):
            selectionNodes.append( UserSelectionNodes(nodesList[i].getRange(), [nodesList[i].getRange()] , nodesList[i].listItemRef))
        return selectionNodes
    def __repr__(self):
        return json.dumps({"range": self._range, "selectedRanges":self.selectedRanges,"listItemRef":self.listItemRef})

class UserSelection(Document):
    ref = StringField(required=True,unique=True)
    listItemRef = StringField()
    listItem = None
    selectedItems = ListField(StringField()) # what the user selects from the list item , could be a range or a single item or multiple items,
    selectedRanges= ListField(ListField(IntField())) #[[0,0]]
    childSelectionRefs = ListField(StringField()) # list of child UserSelections
    linkedSelections = ListField(StringField())
    rootSelection = ReferenceField('self')
    fromLinkedSelection = ReferenceField('self' , default =None)# from which user selection this is defined
    
    parentItem = ReferenceField(Item)
    parentRange = ListField(IntField())
    parentSelectionRef = StringField()
    isEndPoint = BooleanField()
    @staticmethod
    def CreateNew(listItemRef , parentItem ,parentRange, parentSelectionRef,rootSelection , selectedRanges=None,save = False):
        self = UserSelection()
        self.ref = getUniqueId()
        self.listItemRef = listItemRef
        self.childSelectionRefs = [] # list of child UserSelections
        if(save):
            self.parentItem = parentItem
        self.parentRange = parentRange
        self.parentSelectionRef = parentSelectionRef
        self.isEndPoint = True if not self.getListItem().getChildListRefs() else False
        if(selectedRanges):
            self.selectedRanges = selectedRanges 
        self.selectedItems= []
        '''keep track of root of selection too'''
        if(not rootSelection and save):
            parentSelectionRef = self.parentSelectionRef
            while(parentSelectionRef):
                parentSelection = baseApplication.getUserSelctionDataStore().getUserSelectionByRef(parentSelectionRef)
                if(parentSelection and parentSelection.rootSelection):
                    self.rootSelection =  parentSelection.rootSelection
                    break
                parentSelectionRef = parentSelection.parentSelectionRef
                if(not parentSelectionRef):
                    self.rootSelection = parentSelection 
        elif(save and rootSelection):
            self.rootSelection = rootSelection
                
        #this object is put into db once created and all changes will reflect from there
        if(save):
            self.save()
        return self
    
    
    def updateAvailables(self,updatedRanges):
        pass #TODO
        
    @saveInDb
    def onSelection(self,selectedRanges=None,save=True):
        if(selectedRanges):
            self.setSelectedRanges(selectedRanges)
        #save might be needed
        return self.getNextSelections(save)[0]
        
    def getListItem(self):
        if(self.listItem):
            return self.listItem
        self.listItem = baseApplication.getListItemDataStore().getListItemByRef(self.listItemRef)
        return self.listItem
    ''''for temporary creation of userSelection on clientSide'''
    @staticmethod
    def GetNextSelectionsJson(listItemRef,selectedRanges,parentSelectionRef=None,userSelectionNodePath=None):
        '''userSelectionNodePath is the node path upto the selection before the listItemRef, this parameter will give the list of unavailbles in the childSelectionRefs that resulted '''
        #TODO , delete existing selections from DB
        userSelections = []
        for r in selectedRanges:#we shall define for each ranges selected separately            
            listItem = baseApplication.getListItemDataStore().getListItemByRef(listItemRef)
            for parentItem in listItem.getItemsByRange(*r,save=False):
                for i in listItem.getChildListRefs():
                    childListItem = baseApplication.getListItemDataStore().getListItemByRef(i)
                    path  = childListItem.getParentPath()
                    if(path and path.isValidPath(parentItem)>0):
                        data = UserSelection.CreateNew(i,parentItem,parentItem.getRange(),parentSelectionRef).to_mongo()
                        data["parentItem"]= parentItem.to_mongo()
                        if(userSelectionNodePath!=None):
                            availables, unavailables = baseApplication.getNodeDataStore().findAvailables(None, userSelectionNodePath+[UserSelectionNodes(parentItem.getRange(), [], parentItem.listItemRef)], 0, [], [], [], [])
                            data["availables"]=[availables,unavailables]
                        userSelections.append(data)
        return bson.json_util.dumps(userSelections)
    
    def getNextSelections(self ,save=True):
        if(not self.selectedRanges):return None,None
        #TODO , delete existing selections from DB
        del self.childSelectionRefs[:]
        userSelections = []
        for r in self.selectedRanges:#we shall define for each ranges selected separately            
            for item in self.getListItem().getItemsByRange(*r,save=save):
                self.selectedItems.append(item.ref)
                for listItemRef in self.getListItem().getChildListRefs():
                    childListItem = baseApplication.getListItemDataStore().getListItemByRef(listItemRef)   
                    path  = childListItem.getParentPath()
                    if(path and path.isValidPath(item)>0):
                        temp = UserSelection.CreateNew(listItemRef,item,item.getRange(),self.ref,None,save=save)
                        userSelections.append(temp)
                        self.childSelectionRefs.append(temp.ref)
        return self.childSelectionRefs , userSelections
    
    @saveInDb
    def addLinkedSelections(self,userSelection):
        self.linkedSelections.append(userSelection.ref)

    def getChildSelections(self):
        return self.childSelectionRefs
    
    def __repr__(self):
        return json.dumps({"listItemRef":self.listItemRef,"selected index ranges": self.selectedRanges , "parent range":self.parentRange,"parent selection ref ":None if not self.parentSelectionRef else self.parentSelectionRef.listItemRef})

    def getParentRange(self):
        return self.parentRange
    
    def getParentItem(self):
        return self.parentItem
    
    def getSelectedItems(self):
        return map(baseApplication.getItemDataStore().getItemByRef , self.selectedItems) 

    def getSelectedRanges(self):
        return self.selectedRanges if self.selectedRanges else [] 
    
    @saveInDb
    def setSelectedRanges(self,ranges):
        self.selectedRanges =ranges
    
    @saveInDb
    def setParentRange(self , r):
        self.parentRange = r

class DeliveryStatus(EmbeddedDocument):
    statusCode = IntField() # 1 => bo working on it , 2=> intransit , 3=>delivered , 4=> not delivered
    statusDescription = StringField()
    
    def __init__(self,code,description):
        self.statusCode = code
        self.statusDescription = description
    
'''this is to store user:userlection map with price and transactions , delivery history and other useful'''     
class UserSelectionMap(Document):
    ref= StringField(max_length=20 , required= True)
    userRef = StringField(max_length=20 , required= True)
    user = ReferenceField(User) #duplicate , remove if needed
    userSelectionRef = StringField(max_length=20 , required= True) # the head part of user selection
    userSelection = ReferenceField(UserSelection)
    summary = StringField()
    state = IntField(choices=[0,1,2]) # 1=>bo pending 2=> user pending. 0 indicated settled
    
    deliveryStatus = ListField(DeliveryStatus) 
    autoStatusTimeStamp = DateTimeField() # after server time passes with time stamp , replace it with the below auto status code 
    autoStatusCode = IntField(default=ITEM_DELIVERED) # after auto status time passes , the auto delivery status is updated to this given value
    # if the deliveryStatus[-1]==3 and user tries to replace it wth 4 , it goes to dispute box
    timeStamp = DateTimeField()
    transactions= ListField(ReferenceField(Transaction))
    expiryTimeStamp = DateTimeField()
    @staticmethod
    def CreateNew(userRef, userSelectionRef, userSelection , state=0 , timeStamp=None , expiryTimeStamp=None,transactionId=None,summary=None):
        self = UserSelectionMap()
        self.ref = getUniqueId()
        self.userRef = userRef 
        self.userSelection = userSelection
        self.userSelectionRef = userSelectionRef
        self.state = 0
        self.timeStamp = time.time() if timeStamp else None
        self.expiryTimeStamp = expiryTimeStamp
        if(summary):
            self.summary= summary
        if(self.transactions):
            self.transactions = []
        self.transactions.append(transactionId)
        self.save()
        return self
    
    @saveInDb
    def addDeliveryStatus(self,status,description=""):
        previous = self.getCurrentDeliveryStatus()
        self.deliveryStatus.append(DeliveryStatus(status,description))
        '''TODO:'''
        if status == ITEM_NOT_DELIVERED:
            #raise dispute
            pass


'''
Item data store function here
'''        
class ItemDataStore():
    def getItemByRef(self,ref):
        return Item.object(ref=ref).get(0)


'''
all operations pertaining to listitems as a set , are under this class
if just pertaining to a single listItem , write them in listItem class
'''
class ListItemDataStore():
    def __init__(self):
        self.pathDb={}
        self.listItemsByRef = {}
    
    @commitDirty
    def getPathByListRef(self,parentListItemRef,listItemRef):
        return Path.objects.get(parentListItemRef=parentListItemRef, listItemRef=listItemRef) 
        
    
    @commitDirty
    def getListItemByRef(self,ref):
        return ListItem.objects.get(ref=ref) #self.listItemsByRef.get(ref,None)
    
#     def save(self,listItem):
#         if(not self.getListItemByRef(listItem.ref)):
#             self.listItemsByRef[listItem.ref]=listItem
        
    def remove(self,listItemRef):
        listItem = self.getListItemByRef(listItemRef)
        parentListitem = self.getListItemByRef(listItem.parentListItemRef)
        parentListitem.removeChildLists(listItemRef)
        listItem.delete()
    
    def getListItemsByKeywords(self,parentListItemRef,keyword):
        return ListItem.objects(parentListItemRef=parentListItemRef,searchTags__in = [keyword])
    
    def getListItemsBySearchTags(self,searchTags=[],quantity=100):
        val = "_".join(sorted(searchTags))
        return ListItem.objects(searchTags__in = [val]).get(100)
            
    def getListItemsByBOSearchTags(self , businessOperator , searchTags=[]):
        val = "_".join(sorted(searchTags))
        return ListItem.objects(businessOperator=businessOperator , searchTags__in = [val]).get(100)

    def preSelection(self,listItemRef,rangeList=None,quantity=None):
        pass    
    
    

class NodeDataStore():
    def __init__(self):
        self.nodesListDb = [[],[],[],[],[]]
        self.nodesByRef={}     
        
    @commitDirty
    def getNodesBy(self,parentNodeRef, listItemRef, r, level,strictOverlap=False):
        nodes = []
        if(strictOverlap):
            objects = Node.objects(parentNodeRef=parentNodeRef, level = level , listItemRef = listItemRef).filter(Q(rangeVal__a__not__gte=r[1]) & Q(rangeVal__b__not__lte=r[0]) )
        else:
            objects = Node.objects(parentNodeRef=parentNodeRef, level = level , listItemRef = listItemRef).filter(Q(rangeVal__a__not__gt=r[1]) & Q(rangeVal__b__not__lt=r[0]) )
        for node in objects:
            nodes.append(node)
        return nodes

    @commitDirty
    def getNodesByParentAndListItemRef(self,parentNodeRef , listItemRef,level):
        nodes = []   
        for node in Node.objects(parentNodeRef=parentNodeRef, level = level , listItemRef = listItemRef):
            nodes.append(node)
        return nodes
        
    @commitDirty
    def getNodeByRef(self,ref):
        if(ref):
            return Node.objects(ref=ref).get(0)
        return None
    
    def insertNewNodeInDb(self,parentNodeRef, listItemRef , r, level):
        node = Node.CreateNew(listItemRef, r,parentNodeRef,level)
        ##Not needed
        parentNode = self.getNodeByRef(parentNodeRef)
        if(parentNode):
            parentNode.appendChildNodes(node.ref)
        #not-needed
        return node
    
    @commitDirty
    def _removeNodeSingle(self,nodeRef, level):
        node = self.getNodeByRef(nodeRef)
        node.delete()
    
    def removeNodeInDb(self,nodeRef , level):
        node = self.getNodeByRef(nodeRef)
        for i in node.getChildNodes():
            self.removeNodeInDb(i, level)
        #remove from the db
        self._removeNodeSingle(nodeRef, level)
        #remove parent of it is the only child #TODO and verify
        parentNode = self.getNodeByRef(node.parentNodeRef)
        parentNode.removeChildNodes(nodeRef)
        if(not parentNode.getChildNodes()):
            self._removeNodeSingle(parentNode.ref, level)

    def cloneTree(self,node1,parentNodeRef, level): #level says in which db should we insert effectively , return Node object 
        node = self.insertNewNodeInDb(parentNodeRef, node1.listItemRef, node1.getRange(), level)
        for i in node1.getChildNodes():
            self.cloneTree( self.getNodeByRef(i), node.ref ,level )
        return node
        
    
    def checkAndPromoteUnavailable(self,nodesList,childListItemRef):#implies the list item refered by childListItemRef is entirely consumed from the node path indicated by nodeList
        parentNode = nodesList[-1] if len(nodesList)>0 else None
        if(not parentNode):
            return
        
        parentListItem = baseApplication.getListItemDataStore().getListItemByRef(parentNode.listItemRef)
        parentItem = parentListItem.getItemsByRange(*parentNode.getRange())[0]#should defnitely a single item
        parentNode.addFilledChildLists(childListItemRef)
        isParentFilled = True
        for listItemRef in parentListItem.getChildListRefs():#check if all child items are filled
            path = baseApplication.getListItemDataStore().getListItemByRef(listItemRef).getParentPath()
            if(path and path.isValidPath(parentItem)==1):
                if(not parentNode.checkAndGetFilledChildListitems(listItemRef)):#not valid path is not filled
                    isParentFilled = False
        if(isParentFilled):
            return True
        return False
    
    '''This does the reverse way of the groupUnAvailables , splits the unavailable as much possible, this is also recursive'''
    def removeUnAvailables(self, parentNodeRef , userSelectionNodeList , index , level, nodesList): #intially nodes = null
        if(index > level-1):
            #check if we can merge with the parent child selections or create a new one , if merging , check if that level is filled
            selection = userSelectionNodeList[index]
            listItemRef = userSelectionNodeList[index].listItemRef
            for r in selection.getSelectedRanges():
                nodes = self.getNodesBy(parentNodeRef, listItemRef , [r[0]-1,r[1]+1], level)#just pruning instead of checking all
                for node in nodes:
                    x , y , z = BaseApplication.getRangeDiff(node.getRange(), r)
                    if(y):
                        if(x): 
                            if(nodesList): nodesList[-1].removeFilledChildLists(listItemRef)#remove from parent node filledchildlists current listitemref , because its now split 
                            temp = x.pop()
                            node.setRange(temp)
                            for i in x:
                                node1 = self.cloneTree(node, parentNodeRef, level)
                                node1.setRange(i)
                        else:#the entire node is removed , so should check , if we need to remove the parent by checking all its parent nodes
                            self.removeNodeInDb(node.ref, level)
            return True
        
        listItemRef = userSelectionNodeList[index].listItemRef
        r = userSelectionNodeList[index].getRange()
        nodes = self.getNodesBy(parentNodeRef, listItemRef, r, level)
        #log.info(nodes)
        for node in nodes:       
            x , y , z  = BaseApplication.getRangeDiff(node.getRange() , r)
            intersectionNodes = []
            if(x):
                temp = x.pop(0)
                for i in x: #  remaining part of node range after removing intersection
                    nodeCloned = self.cloneTree(node, parentNodeRef, level)
                    nodeCloned.setRange([i[0],i[1]])
                node.setRange(temp)
                for i in y:
                    nodeCloned = self.cloneTree(node, parentNodeRef, level)
                    nodeCloned.setRange([i[0],i[1]])
                    intersectionNodes.append(nodeCloned)
            else:#else total intersection
                intersectionNodes  = [node] # reusing existing node as the intersection node because cloning the same node again is waste
                
            for node1 in intersectionNodes:
                nodesList.append(node1)
                self.removeUnAvailables(node1.ref, userSelectionNodeList, index+1, level,nodesList)
                nodesList.pop()
    #        userSelectionNodeList[index].setRange(z[0] if z else None)
    
    
    '''looks like super recursive , carefully understand ,    split and clone the nodes whenever possible , 
        and inserts until the userSelections are completely exhausted and inserted into the place , cloning nodes is also recursive'''
    def groupUnAvailables(self,parentNodeRef , userSelectionNodeList , index , level, nodesList): #intially nodes = null
        if(index > level-1):
            #check if we can merge with the parent child selections or create a new one , if merging , check if that level is filled
            selection = userSelectionNodeList[index]
            pathsJoined = None
            listItemRef = userSelectionNodeList[index].listItemRef
            for r in selection.getSelectedRanges():
                nodes = self.getNodesBy(parentNodeRef, listItemRef , [r[0]-1,r[1]+1], level)#just pruning instead of checking all
                for node in nodes:
                    pathsJoined = BaseApplication.getIfRangeJoinable(r, node.getRange())
                    #TODO : id there is an overlap , return false , it means , this is not possible
                    if(pathsJoined):
                        node.setRange(pathsJoined)
                if(not pathsJoined): # no path joined nor complete
                    self.insertNewNodeInDb(parentNodeRef, selection.listItemRef, r, level)           
                
            allNodes = self.getNodesByParentAndListItemRef(parentNodeRef, listItemRef, level)#all child nodes
            if(BaseApplication.equivalentRangeLists([ node.getRange() for node in allNodes] , baseApplication.getListItemDataStore().getListItemByRef(listItemRef).getRangeList())):#if entire range list is used up , also this can instead checked upon joining paths 
                if(self.checkAndPromoteUnavailable(nodesList, listItemRef)): #the parent item is  filled in this level
                    #groupUnAvailables(parentNodeRef, userSelectionNodeList, index, level-1, [])
                    log.info("Promote to next level") 
                    log.info(nodesList)
                    self.groupUnAvailables(None, UserSelectionNodes.fromNodes(nodesList), 0, level-1, [])
                    
            return True
        tempRangeSaved = userSelectionNodeList[index].getRange()
        while(not BaseApplication.isEmptyRange(userSelectionNodeList[index].getRange())):
            listItemRef = userSelectionNodeList[index].listItemRef
#            print len(userSelectionNodeList) , index
            r = userSelectionNodeList[index].getRange()
            nodes = self.getNodesBy(parentNodeRef, listItemRef, r, level,True)
            if(not nodes):
                nodes =[self.insertNewNodeInDb(parentNodeRef, listItemRef, r, level)]
            #log.info(nodes)
            for node in nodes:          
                x , y , z  = BaseApplication.getRangeDiff(node.getRange() , r)
                if(x and y):
                    for i in x: #intersection  +  remaining part of node range
                        nodeCloned = self.cloneTree(node, parentNodeRef, level)
                        nodeCloned.setRange([i[0],i[1]])
                if(y):
                    node.setRange(y[0])
                
                nodesList.append(node)
                self.groupUnAvailables(node.ref, userSelectionNodeList, index+1, level,nodesList)
                nodesList.pop()
                userSelectionNodeList[index].setRange(z[0] if z else None)
        userSelectionNodeList[index].setRange(tempRangeSaved)
    
    def findUnavailableNodes(self,parentNodeRef , userSelectionNodeList , index , level, nodesList, unavailableRangeList, betweenRange = []): #intially nodes = null
        if(index > level-1):
            #check if we can merge with the parent child selections or create a new one , if merging , check if that level is filled
            listItemRef = userSelectionNodeList[index].listItemRef #Note: at the last node , to check which are available and not , the userSelectionNodeList witll contain the listRef for which we are checking  
            allChildNodes=[]
            if(betweenRange):
                allChildNodes = self.getNodesBy(parentNodeRef, listItemRef , betweenRange, level)#just pruning instead of checking all
            else:
                allChildNodes = self.getNodesByParentAndListItemRef(parentNodeRef, listItemRef, level)
            for i in allChildNodes:
                BaseApplication.insertRange(unavailableRangeList, i.getRange()) 
            return True
        listItemRef = userSelectionNodeList[index].listItemRef
#        print len(userSelectionNodeList) , index
        r = userSelectionNodeList[index].getRange()
        nodes = self.getNodesBy(parentNodeRef, listItemRef, r, level,True)
        #log.info(nodes)
        for node in nodes:
            x , y , z  = BaseApplication.getRangeDiff(node.getRange() , r)
            if(y): #intersection 
                nodesList.append(node)
                self.findUnavailableNodes(node.ref, userSelectionNodeList, index+1, level,nodesList,unavailableRangeList)
                nodesList.pop()
                #userSelectionNodeList[index].setRange(z[0] if z else None)
    
    def findAvailables(self,parentNodeRef , userSelectionNodeList , index , nodesList, unavailableRangeList=[], betweenRange = [], minimumUnAvailable=[]):
        listItemRangeList = baseApplication.getListItemDataStore().getListItemByRef(userSelectionNodeList[-1].listItemRef).getRangeList()
        unavailableRangeList = []
        level = len(userSelectionNodeList)-1
        self.findUnavailableNodes(parentNodeRef , userSelectionNodeList , index , level, nodesList, unavailableRangeList)
#        print unavailableRangeList , listItemRangeList #subtraction gives me all unavailables and availables
    
    
    def printAllUnavailableNodes(self):
        level = {}
        
        for i in Node.objects():
            if(not level.get(i.level,None)):
                level[i.level] = [i]
            else:
                level[i.level].append(i)
        
#        for i in level.keys():
#            for j in level[i]:
#                print j.__repr__()
#            print "######"+str(i)
            
class UserSelectionDataStore():
    def __init__(self):
        pass
    
    @commitDirty
    def getUserSelectionByRef(self,ref): 
        return UserSelection.objects(ref=ref).get(0)
                
    def isSelectionValid(self,userSelection):#all end points should be reached
        #TODO : check if all the paths have been reached
        return False
    
    '''update the unavailables after transaction completes'''
    def dfsUserSelectionsAndInsert(self,userSelection,path=[]):#userSelection tree
        path.append(userSelection)
        if(userSelection.isEndPoint and userSelection.getSelectedRanges()):#end path
            self.setAsUnavailable(path)
            #commit selections to db TODO here
        
        for selectionRef in userSelection.getChildSelections():
            selection = self.getUserSelectionByRef(selectionRef)
            self.dfsUserSelectionsAndInsert(selection, path)
        path.pop()
    
    def setAsUnavailable(self,userSelectionList , userSelectionNodeList=None):
        if(not userSelectionNodeList):userSelectionNodeList = UserSelectionNodes.fromUserSelections(userSelectionList)

        baseApplication.getNodeDataStore().groupUnAvailables(None, userSelectionNodeList, 0, len(userSelectionNodeList)-1, [])
        
    ''' remove an userSelection Recursively from Db'''
    def dfsUserSelectionsAndRemove(self,userSelection,path=[],forceIndex=True):
        path.append(userSelection)
        if(userSelection.isEndPoint and userSelection.getSelectedRanges() and forceIndex):#end path
            self.setAsAvailable(path)
        
        for selectionRef in userSelection.getChildSelections():
            selection = self.getUserSelectionByRef(selectionRef)
            self.dfsUserSelectionsAndRemove(selection, path)
            del selection # from db
        del userSelection#from db
        path.pop()
                
    ''' this is remove nodes from the nodestore '''
    #temp db remove the nodes from the unavailable list
    def setAsAvailable(self,userSelectionList , userSelectionNodeList=None):
        if(not userSelectionNodeList): userSelectionNodeList= UserSelectionNodes.fromUserSelections(userSelectionList)
        level = len(userSelectionNodeList)
        for i in range(level): #remove in each level
            baseApplication.getNodeDataStore().removeUnAvailables(None, userSelectionNodeList,0,i,[])
        
        

    def editUserSelection(self,userSelectionRef,selectedRanges):    
        #getUserSelectionByRef
        #getSelectedRange , and update and remove and add appropriately
        #getPathfromparentListItemRef
        pass
                
    def evalutateUserSelection(self,userSelection,path=[] , updateItem = False,price=0 , finalPrice=[0,False]):
        path.append(userSelection)
        if(userSelection.isEndPoint and userSelection.getSelectedRanges()):#end path
            for item in userSelection.getSelectedItems():
                price+=item.getPrice()
                if(item.isApprovalNeeded):
                    finalPrice[1]=True

            finalPrice[0]+=price
            if(updateItem): # update item , if needed
                if(finalPrice[1]):
                    item.updateApproval(False)
                item.updatePrice(0)
            
            
            
            #commit selections to db TODO here
            
        
        for selectionRef in userSelection.getChildSelections():
            selection = self.getUserSelectionByRef(selectionRef)
            parentItem = selection.getParentItem()
            price1 = price + 0 if parentItem==None else parentItem.getPrice() 
            
            if(item.isApprovalNeeded):
                finalPrice[1]=True
            if(updateItem): # update item , if needed
                if(finalPrice[1]):#if approval needed , so we remove it now and update state accoringlty
                    item.updateApproval(False)
                item.updatePrice(0)

            
            self.evalutateUserSelection(selection, path, updateItem , price1,finalPrice)
            
        path.pop()
    
    '''before this, update the userSelection as needed'''
    def boOnStatusUpdate(self,userRef, userSelectionRef, approval):#business operator on status update
        userSelection = self.getUserSelectionByRef(userSelectionRef)
        finalPrice = [0,False]
        self.evalutateUserSelection(userSelection,updateItem = False,finalPrice = finalPrice)
        totalPrice , isApprovalNeeded = finalPrice
        baseApplication.getUserSelectionMapDataStore().insertUserSelection(userRef , userSelection , None,APPROVAL_NEEDED_USER, None, None) #current time stamp
            
                

        
    '''before payment , final cart ->checkout page'''
    def beforePayment(self,userSelection):
        finalPrice = [0,False]
        self.evalutateUserSelection(userSelection,updateItem = False,finalPrice = finalPrice)
        totalPrice , isApprovalNeeded = finalPrice
        
        
    '''after payent update the values in userselections and will move to buying history of that User'''
    def onPayment(self,userRef, userSelection , dataStore=None, transaction=None):
        if(dataStore!=self):#might need to move from a different temp store 
            userSelection = self.cloneUserSelectionsIntoDb(userRef, userSelection.ref, None, dataStore)
        finalPrice = [0,False]
        self.evalutateUserSelection(userSelection,updateItem = True,finalPrice = finalPrice)
        price , state = finalPrice
        if(state):#business operator approval needed
            baseApplication.getUserSelectionMapDataStore().insertUserSelection(userRef , userSelection , None,APPROVAL_NEEDED_BO, None, None) #current time stamp 
        else:
            baseApplication.getUserSelectionMapDataStore().insertUserSelection(userRef , userSelection , None,USER_ITEM, None, None) #current time stamp
        
    '''looks like this is needed when we have a temp data store for users selections , which we are removing it now'''
    def cloneUserSelectionsIntoDb(self,userRef, userSelectionRef , dataStore, parentRef  ):#from different userSelectionDataStore like a different dirty table
        userSelection = dataStore.getUserSelectionByRef(userSelectionRef)
        clonedUserSelection = UserSelection.CreateNew(userSelection.listItemRef, userSelection.parentItem, userSelection.parentRange, userSelection.parentSelectionRef, parentRef, userSelection.selectedRanges,save=True)
        for ref in userSelection.getChildSelections():
            self.cloneUserSelectionsIntoDb(userRef, ref,  dataStore, clonedUserSelection.ref)
        return clonedUserSelection    


# This is a summarization of user items that are bought
class UserSelectionMapDataStore():
    def __init__(self):
        pass
        
    def getUserSelectionMapByRef(self,ref):
        return UserSelectionMap.objects(ref=ref).get(0)
    
    def getByUserSelectionFromMap(self,userRef , userSelectionRef):
        return UserSelectionMap.objects(userRef=userRef,userSelectionRef=userSelectionRef).get(0)
        
    def insertUserSelection(self,userRef , userSelection , trasaction,state, timeStamp,expiryTimeStamp = None):
        a = UserSelectionMap(userRef, userSelection.ref, userSelection, state, timeStamp, expiryTimeStamp )
        return a

class Cart(Document):
    userRef = StringField()
    userSelection = ReferenceField(UserSelection)
    summary = StringField(default="Happy Shopping !")
    expiryTime = DateTimeField(required=True)
    price = IntField(default=0)
    approvalNeeded = BooleanField(default=False)
    transactionPending = BooleanField(default=False)
    
class CartDataStore():
    def __init__(self):
        pass
    def remove(self,cart):
        cart.userSelection
        del cart
        
def Preselection(Document):
    ref = StringField()
    listItemRef = ReferenceField(ListItem)
    selection  = DictField() # {listItem : range } , if it reaches an end point , then its preselected , else we shall show the partial selection with childSelectionRefs to load and user to select 
    price = IntField()

class BaseApplication():    
    nodeDataStore = None
    userSelectionDataStore = None
    listItemDataStore = None
    userSelectionMapDataStore  = None
    
    dbUpdateFlag = False #update instantly
    dbUpdateQueueObjects = None
    def __init__(self):
        self.listItemDataStore = ListItemDataStore()
        self.userSelectionTempDataStore = UserSelectionDataStore() 
        self.userSelectionDataStore = UserSelectionDataStore()
        self.userSelectionMapDataStore = UserSelectionMapDataStore()
        self.nodeDataStore = NodeDataStore()
        self.itemDataStore = ItemDataStore()
        self.dbUpdateFlag = False
#         class LockedSet(set):
#             """A set where add() and remove() are thread-safe"""
#             def __init__(self, *args, **kwargs):
#                 self._lock = Lock()
#                 super(LockedSet, self).__init__(*args, **kwargs)
#         
#             def add(self, elem):
#                 with self._lock:
#                     super(LockedSet, self).add(elem)
#         
#             def remove(self, elem):
#                 with self._lock:
#                     super(LockedSet, self).remove(elem)
                    
        self.dbUpdateQueueObjects = deque()

    #Eg:[(1,2),(5,9)] (4,7) -> [(1,2),(4,9)]
    
    def saveInDb(self , obj):
        if(self.dbUpdateFlag):
            obj.save()
        else:
            self.dbUpdateQueueObjects.append(obj)
            
    def commitQueueToDb(self):
        saved = set()
        while(self.dbUpdateQueueObjects):
            count = 0             
            while(count < 100 and self.dbUpdateQueueObjects):  
                count +=1
                saved.add(self.dbUpdateQueueObjects.pop())
                
            while(saved):
                saved.pop().save()
                
    @staticmethod
    def insertRange(a,b):#gap filling type #assume they are in order/sorted by first key
        #TODO: Vinay , a is a rangeList , b is a simple range , merge b into the rangeList and return the rangeList a 
        i = 0
        c , d = b 
        x = len(a)
        state1 =1
        state2 = 0
        p = q = -1
        while(i < x):# replace these two with bin search
            e , f = a[i]
            if(e <= c and c<=f):
                state1 = 0
                break
            elif(c < e):
                state1 = 1 
                break
            i+=1
        p=i
        if(p == x):
            a.append([c,d])
            return a
            
        while(i<x):
            e , f = a[i]
            if(e <= d and d <= f):
                state2 = 1
                break
            elif(d<e):
                state2 = 0 
                break
            i+=1
        q = i 
        
        if(state1==0):
            if(state2==0):#falls before q
                a[p][1]=d
            elif(state2== 1):#falls inside q
                a[p][1]= a[q][1]
            for t in range(p+1,q+state2):
                a.pop(p+1)
                    
        if(state1==1):
            if(state2==0):
                a.insert(p,[c,d])
                p+=1
                q+=1
            elif(state2==1):
                a[q][0] = c
            for t in range(p ,q-state2):
                a.pop(p)
        
        return a
    
    @staticmethod
    def getIfRangeJoinable(r1,r2):
        # Returns joined range if possible else returns None
        if (not (r1 and r2)):
            print "ERR: None Element Cannot be passed to getIfRangeJoinable"
            return None
        a , b = r1
        c , d = r2
        if (a > c):
            a , b = r2
            c , d = r1
        x , y, z = BaseApplication.getRangeDiff(r1, r2)
        if(y or c==b):
            return [a,d] if d>b else [a,b]
        return None
    
    @staticmethod
    def equivalentRangeLists(r1,r2):
        # Assuming ranges are minified ie., [1,3],[3,5] are clubbed into [1,5] form before itself
        r1 = sorted(r1,key=lambda x: x[0])
        r2 = sorted(r2,key=lambda x: x[0])
        
        if(len(r1)!=len(r2)):
            return False
        i=0
        while(i<len(r2)):
            if(r1[i] != r2[i]):
                return False
            i+=1
        return True
        
            
    @staticmethod
    def isSubsetRange(r1,r2):
        x , y , z = BaseApplication.getRangeDiff(r1, r2)
        if(not x and y):
            return True
        return False
    
    '''remember order is important here '''
    @staticmethod
    def getRangeDiff(r1 , r2):
        #code neatly TODO
        if(not r1 and not r2):
            return [],[],[]
        if(not r1):
            return [],[], [r2]
        if(not r2):
            return [r1],[],[]
        
        a , b = r1
        c , d = r2
        if(b<=c or d<=a):
            return [(a,b)], [],[(c,d)] 
            
        if(a<c):
            if(b < d):
                return [(a,c)],[(c,b)],[(b,d)]
            elif(b==d):
                return [(a,c)],[(c,b)],[] 
            else:
                return [(a,c),(d,b)] ,[(c,d)], []
        if(a==c):
            if(b < d):
                return [],[(a,b)],[(b,d)]
            elif b==d:
                return [],[(a,b)],[]
            else:
                return [(d,b)],[(a,d)],[]
        if(a>c):
            if(b<d):
                return [],[(a,b)],[(c,a),(b,d)]
            elif b==d:
                return [],[(a,b)],[(c,a)]
            else:
                return [(d,b)],[(a,d)],[(c,a)]
            
    @staticmethod
    def binary_search_range(a, x, y=None, lo=0, hi=None,key=None):
        # For indexToItemMap
#        print a,x,y
        if (not y):
            y = x + 1
        if hi is None:
            hi = len(a)-1
        while True:
            mid = (lo+hi)//2
            midval = a[mid]
#            midval1=None
#            if(mid+1<len(a)):
#                midval1 = a[mid+1]
            if(key):
                midval = key(a[mid])
#                if(mid+1<len(a)):
#                    midval1 = a[mid+1]

#             if(len(midval)==1):                
#                 step = midval[0]+1
#                 if(midval1):
#                     step = midval1[0] 
#                 midval = (midval[0],step)
                
            if midval[1] <= x:
                lo = mid+1
                if(hi==lo):
                    continue
            elif midval[0] > y: 
                hi = mid
            else:
                p,q,r = BaseApplication.getRangeDiff(midval, [x,y])
                if(q and not r): #total overlap
                    return mid
                return -1
            if (lo >= hi):
                break        
        return -1
        
    @staticmethod
    def isEmptyRange(r):
        if(not r):
            return True
        if(r[1]-r[0] > 0):
            return False
        return True    
    
    
    def getListItemDataStore(self):
        return self.listItemDataStore
    
    
    def getUserSelctionDataStore(self):
        return self.userSelectionDataStore
    
    def getUserSelectionMapDataStore(self):
        return self.userSelectionMapDataStore
    
    def getNodeDataStore(self):
        return self.nodeDataStore
    
    def getItemDataStore(self):
        return self.itemDataStore
    
import logging    
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='/tmp/bookingmanager.log',
                    filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)


log = logging.getLogger('bmv1:')
baseApplication = BaseApplication()


if __name__ == '__main__':
    pass
