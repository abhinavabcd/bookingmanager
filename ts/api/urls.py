from api.ApiIndexHandler import ApiIndexHandler
import tornado.web

def getWebContext():
	prefix = r"/api"
	context = []
	context.append((prefix + '/', ApiIndexHandler),)
	return context
