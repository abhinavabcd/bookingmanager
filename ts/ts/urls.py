from ts.IndexHandler import IndexHandler
import api.urls
import aa.urls



def getWebContext():
	contextList = []
	contextList.append((r'/', IndexHandler),)
	contextList.extend(api.urls.getWebContext())
	contextList.extend(aa.urls.getWebContext())
	return contextList

