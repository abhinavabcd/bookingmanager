import tornado.web
import os.path
from ts import urls

def _prepareApplication():
	webContext = urls.getWebContext();
	settings = {
		"cookie_secret": "23lk4jb3khvsd09fasdlkf",
		"static_path": "static/"
	}
	application = tornado.web.Application(webContext, 
			**settings);
	return application


def getApplication():
	application = _prepareApplication();
	return application
