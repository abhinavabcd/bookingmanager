from mongoengine import *

connect('BM_EM')

class BrowserMst(Document):
	browser = StringField(required=True)

class ViewLog(Document):
	sess_id = StringField(required=True)
	browserUA = StringField()
	remoteIp = StringField()
	appId = IntField()
	pageId = IntField()


