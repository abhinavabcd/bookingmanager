from aa.AccessHandler import AccessHandler
import tornado.web

def getWebContext():
	prefix = r"/aa"
	context = []
	context.append((prefix + '', AccessHandler),)
	return context
