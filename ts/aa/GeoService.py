# documentation at https://pypi.python.org/pypi/geoip2
import geoip2.database

reader = geoip2.database.Reader('assets/GeoList2-City.mmdb')

def getInfoForIp(ipAddress):
	if ipAddress is None:
		return None
	response = reader.city(ipAddress)
	return response
