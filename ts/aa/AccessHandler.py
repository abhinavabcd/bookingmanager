from arch.base import BaseHandler
from aa.AccessModel import *

class AccessHandler(BaseHandler):
	def processRequest(self, request, data):
		## take care of http version
		ip = request.remote_ip
		ua = request.headers['User-Agent']
		lang = request.headers['Accept-Language']
		ref = request.headers['Referer']
		log = ViewLog(sess_id = "sample", browserUA = ua, remoteIp = ip)
		log.save()

	def get(self, *args, **kwargs):
		self.processRequest(self.request, '')
		self.write('{response: "sucess"}')
	
	def post(self, *args, **kwargs):
		self.get(self, *args, **kwargs)
