import tornado.ioloop
import tornado.web
import ts.application

application = ts.application.getApplication()

if __name__=="__main__":
	print """
	Starting application on port 8888
	"""
	application.listen(8888)
	tornado.ioloop.IOLoop.instance().start()
