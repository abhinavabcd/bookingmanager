'''
Created on Apr 29, 2014

@author: abhinav
'''

from mongoengine import *
from Config import *
import json
import time
import datetime 
from sets import Set
from  collections import deque
from threading import Lock
import bson
import itertools

db = connect('bizup')
db.drop_database('bizup')

class A(Document):
    d = DictField()

class B(Document):
    d = ListField()

def test():
    A(d={"1":["asdas"],"2":["asdasd"]}).save()
    a = A.objects().get(0)
    print a.d
    del a.d
    a.save()
    a = A.objects().get(0)
    print a.d
    
def test2():
    B(d=[1,3,4]).save()
    b = B.objects().get(0)
    del b.d[0]
    print b.d
    b.save()
    b = B.objects().get(0)
    print b.d
    
    
    
test()